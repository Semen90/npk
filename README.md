# NPK-калькулятор
---
![GitHub Logo](http://78.155.207.116:7333/dist/images/probe.png)
**Адрес сайта http://78.155.207.116:7333/**
> Проект задумывался как **учебный**, для приобретения и улучшения навыков работы с нижеуказанными инструментами.
>
> Был разработан калькулятор для подсчета микро- и макроэлементов в растворе удобрений.

## Стэк
### Frontend:
- React
- Redux, Redux Thunk
- SCSS, responsive design
- Webpack

### Backend:
- Node, Express.js (MVC)
- Mongo, MongoClient
- JWT authentication
- bcrypt

## TODOs:
---
0. Рефакторить!
1. Server Side Rendering
2. Редактирование статей пользователей
3. Поддержка английского языка (переключатель)
4. Добавить обучащий материал по работе с калькулятором
5. Custom scrollbar