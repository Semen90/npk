var db = require('../db.js');
var ObjectId = require('mongodb').ObjectId;

exports.all = function(userId, cb) {
  // db.get().collection('mixes').find().toArray(function(err, docs){
  //   cb(err, docs);
  // });

  db.get().collection('mixes').aggregate([
    {
      $match: {userId: ObjectId(userId)}
    },  
    {
      $unwind: "$fertilizers"
    },
    {
      $lookup: {
        from: "fertilizer",
        foreignField: "_id",
        localField: "fertilizers.fertilizerId",
        as: "fertilizer"
      }
    },
    {
      $project: {
        _id: 1,
        name: 1,
        fertilizers: {
          fertilizerId: 1,
          fertilizerName: {"$arrayElemAt": ['$fertilizer.name', 0]},
          fullComposition: {"$arrayElemAt": ['$fertilizer.fullComposition', 0] },
          value: 1,
        },
      }
    },
    {
      $group: {
        _id: '$_id',
        name: {$first: "$name"},
        fertilizers: {
          $push: {
            fertilizerId: "$fertilizers.fertilizerId",
            fertilizerName: "$fertilizers.fertilizerName",
            fullComposition: "$fertilizers.fullComposition",
            value: "$fertilizers.value",
          }
        }
      }
    }
  ]).toArray(function(err, docs) {
    cb(err, docs);
  })
}

exports.addMix = function(mix, cb) {
  var formattedMix = { 
    name: mix.name, 
    userId: ObjectId(mix.userId)
  };
  formattedMix.fertilizers = formatMix(mix);

  db.get().collection('mixes').insert({
    name: formattedMix.name,
    userId: formattedMix.userId,
    fertilizers: formattedMix.fertilizers
  }, function(err, result) {
    cb(err, result);
  })
}

// exports.updateMix = function(id, mix, cb) {
exports.updateMix = function(mixData, cb) {
  var formattedMix = {
    name: mixData.name,
    userId: ObjectId(mixData.userId)
  };
  
  formattedMix.fertilizers = formatMix(mixData);

  db.get().collection('mixes').update(
    {_id: ObjectId(mixData.mixId)},
    {
      name: formattedMix.name,
      userId: formattedMix.userId,
      fertilizers: formattedMix.fertilizers
    },
    {},
    function(err, result) {
      cb(err, result);
    }
  )
}

exports.removeMix = function(_id, cb) {
  db.get().collection('mixes').remove({_id: ObjectId(_id)}, function(err, result) {
    cb(err, result);
  })
}

function formatMix(mix) {
  return mix.fertilizers.map(item => {
    item.fertilizerId = ObjectId(item.fertilizerId)
    delete item.userId;
    return item;
  })
}