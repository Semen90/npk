var db = require('../db.js');
var ObjectId = require('mongodb').ObjectId;

exports.all = function(userId, cb){
  // db.get().collection('fertilizer').find().toArray(function(err, docs){
  //   cb(err, docs);
  // });

  db.get().collection('fertilizer').aggregate([
    {
      $match: {userId: ObjectId(userId)}
    },
    {
      $lookup: {
        from: "elements",
        localField: "fullComposition.id",
        foreignField: "_id",
        as: "elementOptions"
      }
    }
  ]).toArray(function(err, docs){
    cb(err, docs);
  })
}

exports.getMainElements = function(cb) {
  db.get().collection('elements').find().toArray(function(err, docs){
    cb(err, docs);
  })
}

exports.addFertilizer = function(newFertilizer, cb) {
  for (let i = 0; i < newFertilizer.fullComposition.length; i++) {
    newFertilizer.fullComposition[i].id = ObjectId(newFertilizer.fullComposition[i].id)
  }

  db.get().collection('fertilizer').insert({
    name: newFertilizer.name,
    userId: ObjectId(newFertilizer.userId),
    fullComposition: newFertilizer.fullComposition
  }, function(err, result){
    cb(err, result);
  });
}

exports.updateFertilizer = function(updatedFertilizer, cb) {
  for (let i = 0; i < updatedFertilizer.fullComposition.length; i++) {
    updatedFertilizer.fullComposition[i].id = ObjectId(updatedFertilizer.fullComposition[i].id)
  }

  db.get().collection('fertilizer').update(
    {_id: ObjectId(updatedFertilizer._id)},
    {$set: {name: updatedFertilizer.name, fullComposition: updatedFertilizer.fullComposition}},
    {},
    function(err, result){
      cb(err, result);
    }
  )
}


exports.deleteFertilizer = function(_id, cb) {
  db.get().collection('fertilizer').remove({_id: ObjectId(_id)}, function(err, result){
    cb(err, result);
  })
}


