var db = require('../db.js');
var ObjectId = require('mongodb').ObjectId;

exports.all = function(userId, cb){
  db.get().collection('agrocultures').aggregate([
    {
      $match: {$or:[{userId: ObjectId(userId)}, {userId: undefined}] }
    }
  ]).toArray(function(err, docs) {
    cb(err, docs)
  })
}

exports.addAgroculture = function(agroculture, cb) {
  let newAgroculture = formatAgroculture(agroculture);

  db.get().collection('agrocultures').insert(newAgroculture, function(err, result){
    cb(err, result);
  })
}

exports.editAgroculture = function(agroculture, cb) {
  let editAgroculture = formatAgroculture(agroculture);

  db.get().collection('agrocultures').update(
    {_id: ObjectId(editAgroculture._id)},
    {
      userId: editAgroculture.userId,
      readOnly: editAgroculture.readOnly,
      name: editAgroculture.name,
      vegetation: editAgroculture.vegetation,
      bloom: editAgroculture.bloom
    },
    {},
    function(err, result){
      cb(err, result);
    }
  )
}

exports.deleteAgroculture = function(_id, cb) {
  db.get().collection('agrocultures').remove({_id: ObjectId(_id)}, function(err, result){
    cb(err, result);
  })
}

function formatAgroculture(agroculture) {
  agroculture.userId = ObjectId(agroculture.userId);
  agroculture.readOnly = agroculture.readOnly;

  if (agroculture.vegetation) {
    for (let i = 0; i < agroculture.vegetation.length; i++) {
      agroculture.vegetation[i].elementName = ObjectId(agroculture.vegetation[i].elementName);
    }
  }

  if (agroculture.bloom) {
    for (let i = 0; i < agroculture.bloom.length; i++) {
      agroculture.bloom[i].elementName = ObjectId(agroculture.bloom[i].elementName);      
    }
  }
  return agroculture;
}