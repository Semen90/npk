var db = require('../db.js');
var ObjectId = require('mongodb').ObjectId;

exports.all = function(cb) {
  db.get().collection('articles').find().toArray(function(err, docs) {
    cb(err, docs);
  });
}

exports.allCategories = function(cb) {
  db.get().collection('articlescategory').find().toArray(function(err, docs) {
    cb(err, docs);
  })
}

exports.add = function(newArticle, cb) {
  db.get().collection('articles').insert({
    title: newArticle.title,
    urlName: newArticle.urlName,
    categoryId: ObjectId(newArticle.categoryId),
    userId: ObjectId(newArticle.userId),
    date: new Date(),
    description: newArticle.description,
    content: newArticle.content
  }, function(err, result) {
    cb(err, result);
  });
}

exports.deleteArticle = function(articleId, cb) {
  db.get().collection('articles').remove({_id: ObjectId(articleId)}, function(err, result) {
    cb(err, result);
  })
}