var db = require('../db.js');
var ObjectId = require('mongodb').ObjectId;

exports.all = function(cb) {
  db.get().collection('users').find().toArray(function(err, docs){
    cb(err, docs);
  });
}

exports.findByLogin = function(login, cb) {
  db.get().collection('users').find({name: login}).toArray(function(err, docs){
    cb(err, docs);
  })
}

exports.findById = function(id, cb) {
  db.get().collection('users').find({_id: ObjectId(id)}).toArray(function(err, docs){
    cb(err, docs);
  })
}

exports.findByLoginAndPassword = function(login, password, cb) {
  db.get().collection('users').find({name: login, password: password}).toArray(function(err, docs){
    cb(err, docs);
  });
}

exports.add = function(user, cb) {
  db.get().collection('users').insert(user, function(err, result){
    cb(err, result);
  })
}

exports.addToken = function(userId, token, cb) {
  db.get().collection('tokens').insert({userId, token}, function(err, result){
    cb(err, result);
  });
}

exports.refreshToken = function(userId, refreshedToken, cb) {
  db.get().collection('tokens').update({userId: ObjectId(userId)}, {$set: {token: refreshedToken}}, {}, function(err, result){
    cb(err, result);
  })
}

exports.getToken = function(token, cb) {
  db.get().collection('tokens').find({token: token}).toArray(function(err, docs){
    cb(err, docs);
  })
}

exports.getTokenByUser = function(userId, cb) {
  db.get().collection('tokens').find({userId: ObjectId(userId)}).toArray(function(err, docs){
    cb(err, docs);
  })
}

exports.removeToken = function(token, cb) {
  db.get().collection('tokens').remove({token: token}, function(err, result){
    cb(err, result);
  });
}

exports.removeTokenByUserId = function(userId, cb) {
  db.get().collection('tokens').remove({userId: ObjectId(userId)}, function(err, result) {
    cb(err, result);
  })
}