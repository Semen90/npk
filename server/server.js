var path       = require('path')
var express    = require('express')
var bodyParser = require('body-parser')
var fallback   = require('express-history-api-fallback')
var morgan     = require('morgan')
var fs         = require('fs')

var router     = require('./routes/router.js')
var jwt        = require('jsonwebtoken') // used to create, sign, and verify tokens
var config     = require('./config') // get our config file


var app        = express();
var publicPath = path.join(__dirname, '..', 'public');

var db = require('./db');

console.log(__dirname)

var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
app.use(morgan('common', { stream: accessLogStream })); // logs

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(publicPath));
app.use(router);
app.use(fallback('index.html', { root: publicPath }));
app.set('port', process.env.PORT || '7333');

app.set('superSecret', config.secret); // secret variable

db.connect(config.database, config.databaseName, function(err){
  if (err) {
    return console.log(err);
  }
  
  console.log('Connected to DB');
  app.listen(app.get('port'), function(){
    console.log('Server http://localhost:' + app.get('port'));
  });
});




