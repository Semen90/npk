var router     = require('express').Router();
var path       = require('path');
var multer     = require('multer')
var fs         = require('fs')

var mkdirp     = require('mkdirp')
var cyrillicToTranslit           = require('cyrillic-to-translit-js')

var articlesCtrl    = require('../controllers/articlesCtrl.js');
var usersCtrl       = require('../controllers/usersCtrl.js');
var authCtrl        = require('../controllers/authCtrl');
var fertilizersCtrl = require('../controllers/fertilizersCtrl.js');
var agrocultureCtrl = require('../controllers/agrocultureCtrl.js');
var authorization   = require('../controllers/authorization.js').authorization;
var mixesCtrl       = require('../controllers/mixesCtrl.js');

// router.use('/articles/', articlesRouter);


//
// ─── INDEX ──────────────────────────────────────────────────────────────────────
//  
router.get('/', function(req, res){
  res.sendFile(path.join(publicPath, 'index.html'));
});

//
// ─── ARTICLES ROUTES ────────────────────────────────────────────────────────────
//

const multerConf = {
  storage: multer.diskStorage({
    destination: function(req, file, next) {
      let path = 'public' + JSON.parse(req.body.content).path;
      console.log('dest path',path)

      if (!fs.existsSync(path)) {
        mkdirp(path, function(err) {next(null, path)})
      } else {
        next(null, path);
      }
    },
    filename: function(req, file, next) {
      next(null, file.originalname);
    }
  })
}

const upload = multer(multerConf)

function clearDir(req, res, next) {
  let path = 'public' + req.get('Path-For-Image');

  if (fs.existsSync(path)) {
    var files = fs.readdirSync(path);
    for (let i = 0; i < files.length; i++) {
      fs.unlinkSync(path + '/' + files[i]);
    }
  }
  next();
}

router.post('/api/new-article', authorization, clearDir, upload.fields([{name: 'image'}]), articlesCtrl.add);
router.get('/api/articles', articlesCtrl.all);
router.get('/api/articles-categories', articlesCtrl.allCategories);
router.delete('/api/delete-article', authorization, articlesCtrl.deleteArticle);

//
// ─── USERS ROUTES ───────────────────────────────────────────────────────────────
//
router.post('/api/registration', usersCtrl.registration);

router.post('/api/auth', authCtrl.auth);
router.post('/api/auth/refreshToken', authCtrl.refreshToken);
router.post('/api/auth/removeToken', usersCtrl.removeTokenByUserId);



//
// ─── FERTILIZER ROUTES ───────────────────────────────────────────────────────────────
//

router.get('/api/fertilizers', fertilizersCtrl.all)
router.get('/api/elements', fertilizersCtrl.getMainElements)
router.post('/api/add-fertilizer', authorization, fertilizersCtrl.addFertilizer)
router.post('/api/update-fertilizer', authorization, fertilizersCtrl.updateFertilizer)
router.delete('/api/delete-fertilizer', authorization, fertilizersCtrl.deleteFertilizer)


//
// ─── AGROCULTURE ROUTES ───────────────────────────────────────────────────────────────
//

router.get('/api/agroculture', agrocultureCtrl.all);
router.post('/api/add-agroculture', authorization, agrocultureCtrl.addAgroculture)
router.post('/api/edit-agroculture', authorization, agrocultureCtrl.editAgroculture)
router.delete('/api/delete-agroculture', authorization, agrocultureCtrl.deleteAgroculture)

//
// ─── MIXES ROUTES ───────────────────────────────────────────────────────────────
//

router.get('/api/mixes', mixesCtrl.all)
router.post('/api/add-mix', authorization, mixesCtrl.addMix)
router.post('/api/update-mix', authorization, mixesCtrl.updateMix)
router.delete('/api/delete-mix', authorization, mixesCtrl.removeMix)

module.exports = router;