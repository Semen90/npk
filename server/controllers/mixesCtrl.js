var Mixes = require('../models/mixes.js');

exports.all = function(req, res) {
  Mixes.all(req.query.userId, function(err, docs) {
    if (err) return res.status(500).send("Ошибка на сервере при получении готовых растворов");
    
    res.send(docs);
  })
}

exports.addMix = function(req, res) {
  var mix = req.body;

  Mixes.addMix(mix, function(err, result) {
    if (err) return res.status(500).send("Ошибка на сервере при добавлении готового раствора");

    res.send(result);
  })
}

exports.updateMix = function(req, res) {
  var mixData = req.body;
  
  Mixes.updateMix(mixData, function(err, result) {
    if (err) return res.status(500).send("Ошибка на сервере при обновлении раствора");

    res.send(result)
  })
}

exports.removeMix = function(req, res) {
  Mixes.removeMix(req.query._id, function(err, result) {
    if (err) return res.status(500).send("Ошибка на сервере при удалении раствора");

    res.send(result);
  })
}


