var jwt       = require('jsonwebtoken')
var usersCtrl = require('./usersCtrl.js')
const authCtrl  = require('../controllers/authCtrl.js')

exports.authorization = function(req, res, next) {
  let userRefreshToken = req.get('Authorization').split(' ')[1];
  let userId = jwt.decode(userRefreshToken).userId;

  usersCtrl.getTokenByUser(userId, function(err, docs) {
    if (err) return res.status(500).send("Ошибка на сервере. Повторите позже");
      
    const token = docs[0].token;

    if (userRefreshToken === token) {
      authCtrl.refreshTokenByAPI(userRefreshToken, function(err, tokens) {
        if (err) return res.status(500).send(err);

        res.set('authorization', JSON.stringify(tokens))
        return next();
      })
    } else {
      return res.status(401).send("Токены не совпадают");
    }
  })
}