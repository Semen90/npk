var Fertilizer = require('../models/fertilizers.js');

exports.all = function(req, res){
  var userId = req.query.userId;

  Fertilizer.all(userId, function(err, docs){
    if (err) return res.status(500).send("Ошибка на сервере при получении удобрений");
    res.send(docs);
  });
  
}

exports.getMainElements = function(req, res) {
  Fertilizer.getMainElements(function(err, docs){
    if (err) return res.status(500).send("Ошибка на сервере при получении элементов");
    res.send(docs);
  })
}

exports.addFertilizer = function(req, res) {
  var newFertilizer = req.body;
  newFertilizer = formatFetrtilizer(newFertilizer);

  Fertilizer.addFertilizer(newFertilizer, function(err, result){
    if (err) return res.status(500).send("Ошибка на сервере при добвлении удобрения");
    res.send(result);
  })
  // res.json(newFertilizer);
}

exports.deleteFertilizer = function(req, res) {
  Fertilizer.deleteFertilizer(req.query._id, function(err, result){
    if (err) return res.status(500).send("Ошибка на сервере при удалении удобрения");
    res.send(result);
  })
}

exports.updateFertilizer = function(req, res) {
  var updatedFertilizer = req.body;
  updatedFertilizer = formatFetrtilizer(updatedFertilizer);

  Fertilizer.updateFertilizer(updatedFertilizer, function(err, result){
    if (err) res.status(500).send("Ошибка на сервере при обновлени удобрения")
    res.send(result)
  });
}

function formatFetrtilizer(newFertilizer) {
  for (let i = 0; i < newFertilizer.fullComposition.length; i++) {
    delete newFertilizer.fullComposition[i].name;
    newFertilizer.fullComposition[i].id = newFertilizer.fullComposition[i]._id;
    delete newFertilizer.fullComposition[i]._id;
  }
  return newFertilizer;
}