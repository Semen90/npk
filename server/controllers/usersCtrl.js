const bcrypt = require('bcrypt');
const config = require('../config.js')
var Users    = require('../models/users.js');

exports.all = function(req, res) {
  Users.all(function(err, docs){
    if (err) {
      console.log(err);
      return res.sendStatus(500);
    }
    res.send(JSON.stringify(docs));
  });
}

exports.findByLogin = function (login, cb) {
  Users.findByLogin(login, function(err, docs){
    cb(err, docs);
  });
}

exports.findById = function(id, cb) {
  Users.findById(id, function(err, docs) {
    cb(err, docs);
  })
}

exports.findByLoginAndPassword = function(login, password, cb){
  Users.findByLoginAndPassword(login, password, function(err, docs){
    cb(err, docs);
  });
}

exports.addToken = function(userId, token, cb){
  Users.addToken(userId, token, function(err, result){
    cb(err, result)
  });
}

exports.refreshToken = function(userId, refreshedToken, cb) {
  Users.refreshToken(userId, refreshedToken, function(err, result){
    cb(err, result);
  });
}

exports.getTokenByUser = function(userId, cb) {
  Users.getTokenByUser(userId, function(err, docs){
    cb(err, docs);
  });
};

exports.getToken = function(token, cb){
  Users.getToken(token, function(err, docs){
    cb(err, docs);
  })
}

exports.removeToken = function(token, cb) {
  Users.removeToken(token, function(err, result){
    cb(err, result);
  })
}

exports.removeTokenByUserId = function(req, res) {
  const userId = req.body.userId;
  Users.removeTokenByUserId(userId, function(err, result) {

    if (err) return res.status(500).send(err);
    
    return res.send(result);
  });
}

exports.registration = function(req, res) {
  var {login, password} = req.body;

  const saltRounds = config.saltRounds

  Users.findByLogin(login, function(err, docs){
    if (err) {
      console.log(err);
      return res.status(500).send("Ошибка на сервере. Попробуйте зарегистрироваться еще раз");
    }
    
    if (docs.length >= 1)  return res.json({isLoginAvailable: false});
    
    bcrypt.genSalt(saltRounds, function(err, salt) {
      if (err) return res.status(500).send("Ошибка на сервере. Попробуйте зарегистрироваться еще раз");
      
      bcrypt.hash(password, salt, function(err, hash) {
        if (err) return res.status(500).send("Ошибка на сервере. Попробуйте зарегистрироваться еще раз");

        var newUser = {
          name: login,
          password: hash,
          admin: false
        };
    
        Users.add(newUser, function(err, result){
          if (err) {
            console.log(err);
            return res.status(500).send("Ошибка на сервере. Попробуйте зарегистрироваться еще раз");
          }  
          console.log('user register now');
          res.sendStatus(200);
        })
      });
    });

  });
}