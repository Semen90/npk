var Articles   = require('../models/articles.js');
var fs         = require('fs');
var path       = require('path');
var rimraf     = require('rimraf');

exports.all = function(req, res) {
  Articles.all(function(err, docs) {
    if (err) res.status(500).send(err);

    res.send(docs);
  })
}

exports.allCategories = function(req, res) {
  Articles.allCategories(function(err, docs) {
    if (err) res.status(500).send(err);
    
    res.send(docs);
  })
}

exports.add = function(req, res) {
  const content = JSON.parse(req.body.content)

  let newArticle = {
    title: content.articleName,
    urlName: content.articleUrl,
    categoryId: content.category._id,
    userId: content.userId,
    description: content.description,
    content: content.html
  }
  
  Articles.add(newArticle, function(err, result) {
    if (err) res.status(500).send('New article error', err);

    res.send(result);
  });
}

exports.deleteArticle = function(req, res) {
  const articleRemoved = JSON.parse(req.query.articleRemoved)
  let path = makePathForDelete(articleRemoved);
  removeDir(path);

  Articles.deleteArticle(articleRemoved.articleId, function(err, result) {
    if (err) res.status(500).send('Delete an article error', err);

    res.send(result);
  })
}
  
function removeDir(path) {
  if (fs.existsSync(path)) {
    rimraf.sync(path);
  }
}

function makePathForDelete(articleRemoved) {
  return `public/images/articles/${articleRemoved.categoryUrl}/${articleRemoved.userId}/${articleRemoved.articleName}`;
}