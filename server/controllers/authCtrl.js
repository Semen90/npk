const bcrypt = require('bcrypt');
var userCtrl   = require('../controllers/usersCtrl.js');
var User       = require('../models/users.js')
var jwt        = require('jsonwebtoken');
var makeTokens = require('../utils.js').makeTokens;
var checkTokensAndGenerateNew = require('../utils.js').checkTokensAndGenerateNew;

exports.auth = function(req, res) {
  let password = req.body.password;

  
  User.findByLogin(req.body.login, function(err, docs) {
    if (err) return res.status(500).send(err);
    
    if (docs.length < 1) return res.sendStatus(401); // Неверный логин

    const user = docs[0];

    bcrypt.compare(password, user.password, function(err, result) {
      if (!result) return res.sendStatus(401); // неверный пароль


      const { _id } = user;

      userCtrl.getTokenByUser(_id, function(err, docs) {
        if(err) return res.status(500).send(err);
        
        userCtrl.findById(_id, function(err, users) {
          if(err) return res.status(500).send(err);
  
          makeTokens(user, function(err, accessToken, refreshToken) {
            if (err) return res.status(500).send(err);
  
            // проверка есть ли уже токены для юзера
            if (docs.length >= 1) {
              userCtrl.refreshToken(_id, refreshToken, function(err, result){
                if (err) return res.status(500).send(err);
  
                return res.json({accessToken, refreshToken});
              });
            }
            else {
              userCtrl.addToken(_id, refreshToken, function(err, result){
                if (err) return res.status(500).send(err);

                return res.json({accessToken, refreshToken});
              });
            } 
          });
        });      
      });

    })
  })
}

exports.refreshToken = function(req, res) {
  const refreshToken = req.body.refreshToken;
  const decodedJwt = jwt.decode(refreshToken);
  const userId = decodedJwt.userId;

  checkTokensAndGenerateNew(userId, refreshToken, function(err, tokens) {
    if (err) return res.send(err);
    
    return res.json(tokens);
  });
}

exports.refreshTokenByAPI = function(refreshToken, cb) {
  const decodedJwt = jwt.decode(refreshToken);
  const userId = decodedJwt.userId;
  
  checkTokensAndGenerateNew(userId, refreshToken, function(err, tokens) {
    cb(err, tokens)
  })
}