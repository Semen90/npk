var Agroculture = require('../models/agroculture.js');

exports.all = function(req, res) {
  var userId = req.query.userId;

  Agroculture.all(userId, function(err, docs){
    if (err) return res.status(500).send("Ошибка на сервере при получении списка культур");
    
    return res.send(docs);
  })
}

exports.addAgroculture = function(req, res) {
  var newAgroculture = req.body;

  Agroculture.addAgroculture(newAgroculture, function(err, result){
    if (err) return res.status(500).send("Ошибка на сервере при добвлении агрокультуры");
    res.send(result);
  })
}

exports.editAgroculture = function(req, res) {
  var editAgroculture = req.body;
  
  Agroculture.editAgroculture(editAgroculture, function(err, result){
    if (err) return res.status(500).send("Ошибка на сервере при редактировании агрокультуры");
    res.send(result);
  })
}

exports.deleteAgroculture = function(req, res) {
  Agroculture.deleteAgroculture(req.query._id, function(err, result){
    if (err) return res.status(500).send("Ошибка на сервере при удалении агрокультуры");
    res.send(result);    
  })
}