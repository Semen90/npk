const jwt        = require('jsonwebtoken');
const config     = require('./config.js');
const userCtrl   = require('./controllers/usersCtrl.js')

function makeTokens(user, cb) {
  let userId = user._id
  let userName = user.name

  jwt.sign({userId: userId, name: userName, exp: 5}, config.secretKey, function (err, accessToken) {
    if (err) return cb(err);
    
    jwt.sign({userId: userId, name: userName, exp: 500000}, config.secretKey, function(err, refreshToken){
      if (err) return cb(err);

      return cb(null, accessToken, refreshToken);
    });
  });
}

function checkTokensAndGenerateNew(userId, refreshToken, cb) {
  userCtrl.getTokenByUser(userId, function(err, docs){
    if (err) return res.status(500).send(err);

    if (docs.length === 1) {

      let tokenFromDb = docs[0];

      if (refreshToken === tokenFromDb.token) {
        userCtrl.findById(userId, function(err, users) {
          if (err) return res.status(500).send(err);
          
          makeTokens(users[0], function(err, accessToken, refreshToken) {
            if (err) return res.status(500).send(err);
            
            userCtrl.refreshToken(userId, refreshToken, function(err, result) {
              if (err) return res.status(500).send(err);
              
              return cb(null, {accessToken, refreshToken});
            });    
          })          
        })        
      }
      else {
        console.log('error 1')
        return cb(new Error('401'))
      }
      
    } else {
      console.log('error 2')
      return cb(new Error('401'))
    } 
  });
}

module.exports = {
  makeTokens: makeTokens,
  checkTokensAndGenerateNew: checkTokensAndGenerateNew
}


