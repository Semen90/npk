const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: './client/src/index.js',
  output: {
    path: path.join(__dirname, 'public', 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/i,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', 'react', 'stage-0']
          }
        }
      },
      {
        test: /\.(css|scss)$/i,
        exclude: /node_modules\/(?!(react-draft-wysiwyg)\/).*/,
        use: [
          {loader: 'style-loader'}, {loader: 'css-loader'}, {loader: 'sass-loader'}
        ]
      },
      {
        test: /\.(eot|ttf|woff)$/i,
        include: path.join(__dirname, "client", "src","fonts"),
        use: {
          loader: 'file-loader',
          options: {
            name: "[path][name].[ext]",
            context: path.join(__dirname, "client/src/fonts"),
            outputPath: '/fonts',
            publicPath: '/dist/fonts'
          }
        }
      },
      {
        test: /\.(png|jpg|svg|gif|ico)$/i,
        include: path.join(__dirname, "client", "src","images"),
        use:[
          {
            loader: 'file-loader',
            options: {
              name: "[path][name].[ext]",
              context: path.join(__dirname, "client/src/images"),
              outputPath: '/images',
              publicPath: '/dist/images'
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65
              },
              // optipng.enabled: false will disable optipng
              optipng: {
                enabled: false,
              },
              pngquant: {
                quality: '65-90',
                speed: 1
              },
              gifsicle: {
                interlaced: false,
              }
            }
          }
        ]          
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], {
        dry: false            
    })
  ]
}