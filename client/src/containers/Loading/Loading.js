import React from 'react'
import './loading.scss'

export default (props) => {
  return (
    <div className="loading">
      <div className="loading-circle">
        <div className="loading-circle__little"></div>
        <div className="loading-circle__inner"></div>
      </div>
    </div>
  )
}
