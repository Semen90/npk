import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavLink, withRouter } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import './header.scss';
import { fetchRemoveTokens } from '../../actions/user';
import { getUserByToken, getUserIdByUser } from '../../utils/utils';

class Header extends Component {
  state = {
    isMenuOpen: false
  }

  onExit = () => {
    if (this.state.isMenuOpen) {
      this.setState({isMenuOpen: false})
    }
    
    const userId = getUserIdByUser(this.props.user)
    this.props.dispatch(fetchRemoveTokens(userId));
    this.props.history.push("/");
  }

  renderUserName =() => {
    if (this.props.tokens) {
      return getUserByToken(this.props.tokens.refreshToken).name
    }
  }

  onMenuButton = () => {
    this.setState({isMenuOpen: !this.state.isMenuOpen})
  }

  onMenuItem = () => {
    if (this.state.isMenuOpen) {
      this.setState({isMenuOpen: false})
    }
  }

  render() {
    console.log(this.state)
    return (
      <header className="header">
        <div className="header__inner">
          <NavLink className="logo" to="/">NPK</NavLink>
          <div className={this.state.isMenuOpen ? "sign-panel _active" : "sign-panel"}>
            <div className="header__username">{this.renderUserName()}</div>            
            <NavLink className="sign-panel__link" activeClassName="_active" onClick={this.onMenuItem} to="/" exact>Главная</NavLink>
            <NavLink className="sign-panel__link" activeClassName="_active" onClick={this.onMenuItem} to="/articles">Статьи</NavLink>
            <NavLink className="sign-panel__link" activeClassName="_active" onClick={this.onMenuItem} to="/calculator">Калькулятор</NavLink>
            {this.props.isAuth ? 
              <span className="sign-panel__link" onClick={this.onExit}>Выйти</span>
              :
              <NavLink className="sign-panel__link" activeClassName="_active" onClick={this.onMenuItem} to="/auth">Войти</NavLink>
            }
            {
              this.props.isAuth ? 
              null
              :
              <NavLink className="sign-panel__link" activeClassName="_active" onClick={this.onMenuItem} to="/registration">Зарегистрироваться</NavLink>
            }            
          </div>
          <div className="header__button" onClick={this.onMenuButton}>
            <i><FontAwesomeIcon icon="bars" /></i>
          </div>
        </div>
      </header>
    );
  }
}

const mapState = (state) => {
  return {
    user: state.user,
    tokens: state.user.tokens,
    isAuth: state.user.isAuth
  }
}

Header.propTypes = {
  user: PropTypes.object.isRequired,
  tokens: PropTypes.object,
  isAuth: PropTypes.bool.isRequired
}

export default withRouter(connect(mapState, null)(Header));