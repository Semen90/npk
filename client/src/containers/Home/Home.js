import React, { Component } from 'react';

class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div className="main-headertext">Добро пожаловать!</div>
        <div className="main-text greeting">
          <p>Используйте NPK-калькулятор, чтобы создать питательные смеси для растений; добавляйте свои удобрения; устанавливайте необходимую дозировку.</p>
          <p>Читайте статьи о растениях и растениеводстве.</p>
          <p>Удачи!</p>
        </div>
      </div>
    );
  }
}

export default Home;