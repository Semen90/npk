import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './popupRemove.scss'

const PopupRemove = (props) => {
  let onRemove = () => {
    props.removableCallback(true);
  }
  
  let onCancel = () => {
    props.removableCallback(false);
  }

  return (
    <div className="popup-remove">
      <div className="popup-remove__text">{props.removeText}</div>        
      <div>
        <button type="button" className="button btn _danger" onClick={onRemove}>Удалить</button>
        <button type="button" className="button btn _secondary" onClick={onCancel}>Отмена</button>
      </div>
    </div>
  )
}

PopupRemove.propTypes = {
  removableCallback: PropTypes.func.isRequired,
  removeText: PropTypes.string.isRequired
}

export default PopupRemove;