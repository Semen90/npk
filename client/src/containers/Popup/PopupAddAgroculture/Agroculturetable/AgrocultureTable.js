import React, { Component } from 'react'
import PropTypes from 'prop-types'

import AgrocultureTableItem from './AgrocultureTableItem'
import './agrocultureTable.scss'

class AgrocultureTable extends Component {
  setElement = (index, element) => {
    let arr = this.props.elements.concat();
    arr[index] = element;

    if (this.props.setVegetation) {
      this.props.setVegetation(arr);
      return;
    }

    if (this.props.setBloom) {
      this.props.setBloom(arr);
      return;
    }
  }
 
  renderElements = () => {
    return this.props.elements.map((item, index) => {
      return (
        <AgrocultureTableItem resetNan={this.props.resetNan} nan={this.props.nan} setElement={this.setElement} key={index} index={index} element={item}/>
      )
    })
  }

  render = () => {
    return (
      <div className="elements-table">
        <div className="elements-table__head">
          <div className="elements-table__cell _1">Элемент</div>
          <div className="elements-table__cell _2">мг/л</div>
        </div>
        {this.renderElements()}
      </div>
    )
  }
  
}

AgrocultureTable.propTypes = {
  elements: PropTypes.array.isRequired,
  setVegetation: PropTypes.func,
  setBloom: PropTypes.func
}

export default AgrocultureTable;