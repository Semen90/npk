import React, { Component } from 'react'
import PropTypes from 'prop-types'
import NumberFormat from 'react-number-format'

export default class AgrocultureTableItem extends Component {
  state = {
    value: ''
  }

  componentWillMount() {
    if (this.props.element) {
      this.setValue(this.props.element.value)     
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.element) {
      this.setValue(nextProps.element.value)      
    }
  }

  setValue = (value) => {
    this.setState({value: value})
  }

  onValueChange = (e) => {
    if (this.props.nan) {
      this.props.resetNan();
    }
    // let value = e.target.value.replace(/,/, '.');
    
    let value = e.target.value;
    this.setState({value: value}, () => {
      this.props.setElement(this.props.index, {
        _id: this.props.element._id,
        name: this.props.element.name,
        value: value
      })
    })
  }

  render() {
    return (
      <div className="elements-table__item">
        <div className="elements-table__cell _1">{this.props.element.name}</div>
        <div className="elements-table__cell _2">
          <NumberFormat isNumericString={true}
            placeholder="0"
            className="elements-table__input"
            type="text"
            value={this.state.value}
            onChange={this.onValueChange} />
        </div>
      </div>
    )
  }
}

AgrocultureTableItem.propTypes = {
  element: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired
}