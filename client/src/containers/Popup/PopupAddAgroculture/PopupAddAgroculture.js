import React, { Component } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'

import { authSuccess } from '../../../actions/user'
import AgrocultureTable from './AgrocultureTable/AgrocultureTable'
import { getOnlySingleElements } from '../../../utils/utils'
import './popupAddAgroculture.scss'
import checkAuthorization from '../../../utils/checkAuthorization'
import { setTokensFromAuthHeader } from '../../../utils/setTokensFromAuthHeader'

export default class PopupAddAgroculture extends Component {

  state = {
    _id: undefined,
    agrocultureName: '',
    vegetation: [],
    bloom: [],
    nan: false
  }

  componentWillMount() {
    this.initialize(getOnlySingleElements(this.props.elements), this.props.editableAgroculture)
  }
  
  componentWillReceiveProps(nextProps) {
    this.initialize(getOnlySingleElements(nextProps.elements), this.props.editableAgroculture)
  }

  initialize = (elements, editable) => {
    if (editable) {
      this.initializeEditable(elements, editable);
    } else {
      this.initializeNew(elements)
    }
  }

  initializeNew = (elements) => {
    let arr = elements.map(item => {
      return {
        _id: item._id,
        name: item.name,
        value: ''
      }
    });
    this.setState({vegetation: arr, bloom: arr});
  }

  getEmptyElementsList = (elements) => {
    return elements.map(item => {
      return {
        _id: item._id,
        name: item.name,
        value: ''
      }
    })
  }
  
  initializeEditable = (elements, editable) => {
    // console.log('editable', editable)
    // console.log(' ')

    let vega = this.getEmptyElementsList(elements);
    let bl = this.getEmptyElementsList(elements);

    vega = this.setEditableElements('vegetation', vega, editable);
    bl = this.setEditableElements('bloom', bl, editable);

    // console.log('vega,', vega)
    // console.log('bloom,', bl)
    
    this.setState({_id: editable._id, agrocultureName: editable.name, vegetation: vega, bloom: bl})
  }

  setEditableElements = (type, emptyList, editable) => {
    if (!type) return;

    let newList = emptyList.slice(0);

    switch(type) {
      case 'vegetation':
        if (editable.vegetation) {
          for (let i = 0; i < newList.length; i++) {
            for (let j = 0; j < editable.vegetation.length; j++) {
              if (newList[i]._id === editable.vegetation[j].elementName) {
                newList[i].value = editable.vegetation[j].value;
              }        
            }      
          }  
          return newList;
        }        
        break;

      case 'bloom':
        if (editable.bloom) {
          for (let i = 0; i < newList.length; i++) {
            for (let j = 0; j < editable.bloom.length; j++) {
              if (newList[i]._id === editable.bloom[j].elementName) {
                newList[i].value = editable.bloom[j].value;
              }        
            }      
          }
          return newList;          
        }
        break;
    }

    return newList;
  } 

  setVegetation = (vegetationList) => {
    this.setState({vegetation: vegetationList});
  }

  setBloom = (bloomList) => {
    this.setState({bloom: bloomList});
  }

  saveAgroculture = () => {
    let { agrocultureName, vegetation, bloom } = this.state;

    if (this.isNanInArray(vegetation)) { // если есть NaN, то выйти
      this.setState({nan: true})
      return;
    }

    if (this.isNanInArray(bloom)) {
      this.setState({nan: true})
      return;
    }

    vegetation = this.formatArrayForDB(vegetation);
    bloom = this.formatArrayForDB(bloom);

    if (agrocultureName && (vegetation.length >= 1 || bloom.length >= 1)) {
      this.sendToServer(agrocultureName, vegetation, bloom);
      return;
    }
  }

  resetNan = () => {
    this.setState({nan: false})
  }

  isNanInArray = (array) => {
    let nan = false;

    array.forEach(item => {
      if (isNaN(item.value)) {
        nan = true;
      }
    });

    return nan;
  }

  formatArrayForDB = (array) => {
    let arr = array.filter(item => {
      return (!isNaN(item.value) && (item.value !== 0));
    });

    return arr.map(item => {
      return {
        elementName: item._id,
        value: item.value
      }      
    })   
  }

  sendToServer = (name, vegetation, bloom) => {
    let newAgroculture = {};
    newAgroculture.name = name;
    newAgroculture.readOnly = false;
    newAgroculture.userId = this.props.user;

    let { refreshToken } = this.props.userTokens
    
    if (vegetation.length >= 1) newAgroculture.vegetation = vegetation;
    if (bloom.length >= 1) newAgroculture.bloom = bloom;
    
    if (this.props.editableAgroculture) {
      newAgroculture._id = this.state._id;
      newAgroculture.readOnly = this.props.editableAgroculture.readOnly;
      
      axios.post('/api/edit-agroculture', newAgroculture, {headers: {'Authorization': `Bearer ${refreshToken}`}})
      .then(response => {
        setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)

        if (response.data.ok === 1) {
          this.props.setTableCulture(newAgroculture);
          this.props.refreshAgrocultures();
        }
      })
      .catch(err => checkAuthorization(err.response.status, this.props, this.props.user))

      return;
    }
    
    axios.post('/api/add-agroculture', newAgroculture, {headers: {'Authorization': `Bearer ${refreshToken}`}})
    .then(response => {
      setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)

      if (response.data.result.ok === 1) {
        this.props.refreshAgrocultures();
      }
    })
    .catch(err => {
      console.log(err);
      checkAuthorization(err.response.status, this.props, this.props.user)
    })
  }

  render() {
    return (
      <form className="form add-agroculture">
        <input className="input-text" type="text"
          placeholder="Название агрокультуры"
          value={this.state.agrocultureName}
          onChange={(e) => this.setState({agrocultureName: e.target.value})} 
        />
        {this.state.nan ? <div className="error mb20">Значения полей таблиц должны быть числа с точкой</div> : null}
        <div className="add-agroculture__box">
          <div className="add-agroculture__part _left">
            <div className="add-agroculture__type _vegetation">Вегетация</div>
            <AgrocultureTable nan={this.state.nan} resetNan={this.resetNan} setVegetation={this.setVegetation} elements={this.state.vegetation}/>
          </div>
          <div className="add-agroculture__part _right">
            <div className="add-agroculture__type _bloom">Цветение</div>
            <AgrocultureTable nan={this.state.nan} resetNan={this.resetNan} setBloom={this.setBloom} elements={this.state.bloom}/>
          </div>
        </div>
        <button type="button" className="button" onClick={this.saveAgroculture}>Сохранить</button>
      </form>
    )
  }
}

PopupAddAgroculture.propTypes = {
  elements: PropTypes.array.isRequired,
  editableAgroculture: PropTypes.object,
  user: PropTypes.string.isRequired,
  userTokens: PropTypes.object,
  setTableCulture: PropTypes.func,
  refreshAgrocultures: PropTypes.func.isRequired
}