import React, { Component } from 'react'
import PropTypes from 'prop-types'


import axios from 'axios'

import checkAuthorization from '../../../utils/checkAuthorization'
import NewElement from './NewElement'
import './popupAddFertilizer.scss';
import { getUserIdByToken } from '../../../utils/utils'
import { setTokensFromAuthHeader } from '../../../utils/setTokensFromAuthHeader';

export default class PopupAddFertilizer extends Component {

  state = {
    fertilizerName: '',
    elements: []
  }

  componentWillMount = () => {
    if (this.props.editableFertilizer) {
      this.initElements(this.props);
      return;
    }

    this.setState({elements: this.arrEmptyElement(this.state.elements)});
  }

  componentWillReceiveProps(nextProps) {
    this.initElements(nextProps);
  }

  initElements = (props) => {
    if (props.editableFertilizer) {
      let elements = props.editableFertilizer.fullComposition.map(item => {
        return {
          _id: item.id,
          name: item.name,
          value: item.value
        }
      })
      this.setState({
        fertilizerName: props.editableFertilizer.name,
        elements: elements
      }) 
    }
  }
  
  

  arrEmptyElement = (currentArr) => {
    let arr = JSON.parse(JSON.stringify(currentArr));
    arr.push({
      _id: undefined,
      name: undefined,
      value: 0
    });
    
    return arr;
  }

  isValidity = () => {
    const composition = this.state.elements.filter(item => item._id !== undefined && item.name !== undefined && item.value !== '')
    if (composition.length >= 1 && this.state.fertilizerName !== '') {
      return {
        status: true,
        composition: composition
      }
    }
    
    return {
      status: false,
      composition: null
    }
  }
  
  saveFertilizer = () => {
    const { status, composition} = this.isValidity(); 
    if (status) {
      this.saveToServer(composition, this.state.fertilizerName);
    }
  }
  
  
  updateFertilizer = () => {
    const { status, composition} = this.isValidity(); 
    if (status) {
      this.updateToServer(composition, this.state.fertilizerName, this.props.editableFertilizer._id);
    }
  }
  
  updateToServer = (composition, name, _id) => {
    const updatedFertilizer = {
      _id,
      userId: this.props.user,
      name,
      fullComposition: composition
    }
    let { refreshToken } = this.props.userTokens;
    const userId = getUserIdByToken(refreshToken);

    axios.post('/api/update-fertilizer', updatedFertilizer, {headers: {'Authorization': `Bearer ${refreshToken}`}})
    .then(response => {
      setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)
      
      if (response.data.ok !== 1) {
        throw new Error();
      }
      this.props.refreshFertilizerList();
    })
    .catch(err => checkAuthorization(err.response.status, this.props, userId))
  }  

  saveToServer = (composition, fertilizerName) => {
    const fertilizer = {
      name: fertilizerName,
      userId: this.props.user,
      fullComposition: composition,
    }
    let { refreshToken } = this.props.userTokens;
    const userId = getUserIdByToken(refreshToken);

    axios.post('/api/add-fertilizer', fertilizer, {headers: {'Authorization': `Bearer ${refreshToken}`}})
    .then(response => {
      setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)

      if (response.data.result.ok !== 1) {
        throw new Error();
      }
      this.props.refreshFertilizerList();
    })
    .catch(err => checkAuthorization(err.response.status, this.props, userId))
  }
  
  removeFertilizer = (index) => {
    let arr = [];
    for (let i = 0; i < this.state.elements.length; i++) {
      if (index !== i) {
        arr.push(this.state.elements[i])
      }      
    }
    this.setState({elements: arr});
  }

  setElementName = (index, element) => {
    let arr = this.state.elements.concat();
    arr[index]._id = element._id;
    arr[index].name = element.name;
    this.setState({elements: arr});
  }
  
  setElementValue = (index, value) => {
    let arr = this.state.elements.concat();
    arr[index].value = value;
    this.setState({elements: arr});    
  }

  addNewElement = () => {
    this.setState({elements: this.arrEmptyElement(this.state.elements)})
  }

  renderElements = () => {
    let elements = this.state.elements.map((item, index) => {
      return (
        <NewElement
          key={index}
          setElementName={this.setElementName}
          setElementValue={this.setElementValue}
          removeFertilizer={this.removeFertilizer}
          index={index}
          currentElement={item}
          elements={this.props.elements}
        />
      )
    })
    return elements;
  }
  
  render() {
    return (
      <form className="form add-fertilizer">
        <input className="input-text" type="text"
          placeholder="Название удобрения"
          value={this.state.fertilizerName}
          onChange={(e) => this.setState({fertilizerName: e.target.value})} 
        />
        <div className="subtitle">Состав удобрения:</div>
        {this.renderElements()}
        <div className="popup__btn" onClick={this.addNewElement}>+</div>
        <div>
          { this.props.editableFertilizer ? 
            <button type="button" className="button" onClick={this.updateFertilizer}>Обновить</button>
            :
            <button type="button" className="button" onClick={this.saveFertilizer}>Сохранить</button>
          }
        </div>
      </form>
    )
  }
}

PopupAddFertilizer.propTypes = {
  editableFertilizer: PropTypes.object,
  user: PropTypes.string.isRequired,
  userTokens: PropTypes.object.isRequired,
  refreshFertilizerList: PropTypes.func.isRequired,
  elements: PropTypes.array.isRequired
}