import React, { Component } from 'react'
import NumberFormat from 'react-number-format'
import PropTypes from 'prop-types'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class NewElement extends Component {
  state = {
    element: {},
    value: '',
    isNan: false
  }

  componentWillMount() {
    this.setDefaultElements(this.props.currentElement);
  }

  componentWillReceiveProps(nextProps) {
    this.setDefaultElements(nextProps.currentElement);
  }

  setDefaultElements = (currentElement) => {
    if (currentElement) {
      this.setState({
        element: {
          _id: currentElement._id,
          name: currentElement.name
        },
        value: currentElement.value === 0 ?  '' : currentElement.value
      })
    }
  }

  renderOptions = () => {
    return this.props.elements.map((item, index) => {
      return <option key={index} value={item._id}>{item.name}</option>
    })
  }

  onSelectChange = (e) => {
    const currentElement = this.props.elements.filter(item => item._id === e.target.value);
    this.setState({
      element: {
        _id: currentElement[0]._id,
        name: currentElement[0].name
      }
    }, () => {this.props.setElementName(this.props.index, this.state.element)});
  }

  onValueChange = (e) => {
    let value = e.target.value;

    this.setState({isNan: false});

    if (isNaN(value)) {
      this.setState({isNan: true});
    } else {
      this.setState({value: e.target.value}, () => {this.props.setElementValue(this.props.index, this.state.value)});
    }    
  }

  removeFertilizer = () => {
    this.props.removeFertilizer(this.props.index);
  }

  render() {
    return (
      <div className="popup-line">
        <select className="select _mw200" value={this.state.element._id} onChange={this.onSelectChange}>
          <option></option>
          {this.renderOptions()}
        </select>
        <NumberFormat className="popup__input" placeholder="0" isNumericString={true} value={this.state.value} onChange={this.onValueChange}/>
        <span className="popup__units">%</span>
        {this.state.isNan ? <div className="error">Введите число с точкой</div> : null}
        <div className="popup__remove" onClick={this.removeFertilizer}><FontAwesomeIcon icon="times-circle" /></div>
      </div>
    )
  }
}

NewElement.propTypes = {
  currentElement: PropTypes.object.isRequired,
  elements: PropTypes.array.isRequired,
  setElementName: PropTypes.func.isRequired,
  setElementValue: PropTypes.func.isRequired,
  removeFertilizer: PropTypes.func.isRequired
}