import React, { Component } from 'react'
import PropTypes from 'prop-types';

import './popup.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { popupsType } from './popups'

export default class Popup extends Component {

  renderPopup = () => {
    if (this.props.popupComponent) {
      return this.props.popupComponent;
    }
  }

  onClose = (e) => {
    this.props.closePopup();
  }
  
  onPopupBack = (e) => {
    return e.target === this.closePopup ? this.onClose() : null;
  }

  setSize = () => {
    switch(this.props.type) {
      case popupsType.SMALL:
        return 'popup__window _small';
        
      default: 
        return 'popup__window _medium';
    }
  }


  render() {

    return (
      <div className="popup" onClick={this.onPopupBack} ref={closePopup => this.closePopup = closePopup}>
        <div className={this.setSize()}>
          <div className="popup__cross" onClick={this.onClose}><FontAwesomeIcon icon="times"/></div>
          <div className="popup__title">{this.props.title}</div>
          <div className="popup__content">
            {this.renderPopup()}
          </div>
        </div>
      </div>
    )
  }
}

Popup.propTypes = {
  title: PropTypes.string.isRequired,
  popupComponent: PropTypes.element.isRequired,
  type: PropTypes.string.isRequired
}