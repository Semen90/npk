import React, { Component } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { connect } from 'react-redux'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'
import FormData from 'form-data'
import { EditorState, convertToRaw, convertFromRaw, ContentState  } from 'draft-js'
import cyrillicToTranslit from 'cyrillic-to-translit-js'
import sanitizeHtml from 'sanitize-html'

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import './newArticle.scss'

import checkAuthorization from '../../utils/checkAuthorization'
import { getUserIdByUser } from '../../utils/utils'
import { setTokensFromAuthHeader } from '../../utils/setTokensFromAuthHeader';

class NewArticle extends Component {
  state = {
    editorState: EditorState.createEmpty(),
    uploadedImages: [],
    category: undefined,

    articleName: '',
    articleNameError: false,

    articleDescription: '',
    articleDescriptionError: false,
    errorMessage: ''
  }

  componentWillMount() {
    this.initCategory(this.props.articlesCategories)

    if (this.props.editableArticle) this.initStateForEdit(this.props.editableArticle);
  }
  
  componentWillReceiveProps(nextProps) {
    this.initCategory(nextProps.articlesCategories)

    if (nextProps.editableArticle) this.initStateForEdit(nextProps.editableArticle);
    
  }
  
  initCategory = (categories) => {
    this.setState({category: categories[0]})
  }

  makeFileNameByUrl = (url) => {
    return url.split('/')[url.split('/').length - 1];
  }

  getImage = (filePath, fileName, index) => {
    return new Promise((resolve, reject) => {

      axios.get(filePath, {responseType: 'blob'})
      .then(res => {
        let newFile = new File([res.data], fileName);
        return newFile;
      })
      .then(newFile => {
        return this._uploadImageCallBack(newFile)
          .then(data => {
            let blobUrl = data.data.link;
            resolve({index, blobUrl});
          })
      })          
      .catch(err => reject(err))
    })
  }

  getFiles = (images) => {
    return new Promise ((resolve, reject) => {
      let blobs = [];
      let returnedResults = 0;

      for (let i = 0; i < images.length; i++) {
        let match = images[i].match(/src\s*=\s*"(.+?)"/g);  
        const filePath = match[0].slice(5, match[0].length - 1)
        const fileName = this.makeFileNameByUrl(match[0])

        this.getImage(filePath, fileName, i)
        .then(result => {
          blobs.push(result)
          returnedResults++;
          if (returnedResults === images.length) { resolve(blobs) }

          return result
        })
        .catch(err => console.error(err))
      }
    })    
  }

  replaceSrcForBlob = (images, blobs) => {
    let arr = [];
    for (let i = 0; i < blobs.length; i++) {
      arr.push(images[i].replace(/src\s*=\s*"(.+?)"/, `src="${blobs[i].blobUrl}"`))
    }
    return arr;
  }

  replaceImagesForNewUrl = (images, imagesWithBlob, html) => {
    let newHtml = html.slice(0);
    for (let i = 0; i < images.length; i++) {
      newHtml = newHtml.replace(images[i], imagesWithBlob[i])
    }
    return newHtml;
  }

  initStateForEdit = (article) => {
    let html = article.content;
    let images = html.match(/<img([\w\W]+?)>/g);
    let imagesWithBlob = [];

    let blobUrls = []
    let result = [];


    if (images.length >= 1) {
      this.getFiles(images)
      .then((blobs) => {
        blobs.sort((a, b) => {
          return a.index > b.index
        })

        imagesWithBlob = this.replaceSrcForBlob(images, blobs);
        html = this.replaceImagesForNewUrl(images, imagesWithBlob, html)
        const contentBlock = htmlToDraft(html);
  
        if (contentBlock) {
          const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
          const editorState = EditorState.createWithContent(contentState);


          this.setState({
            articleName: article.title,
            articleDescription: article.description,
            editorState: editorState,
            category: this.props.getCategoryById(article.categoryId)
          })
        }
      })
      .catch(err => console.error(err, 2))
    }



  }

  _uploadImageCallBack = (file) => {
    let uploadedImages = this.state.uploadedImages;
  
    const imageObject = {
      file: file,
      localSrc: URL.createObjectURL(file),
    }
    uploadedImages.push(imageObject);
    this.setState({uploadedImages: uploadedImages})
    
    return new Promise(
      (resolve, reject) => {
        // console.log('resolve', resolve)
        resolve({ data: { link: imageObject.localSrc } });
      }
    )
  }

  toolbar = {
    options: ['inline', 'blockType', 'list', 'textAlign', 'link', 'image', 'history'],
    inline: {
      inDropdown: false,
      className: undefined,
      component: undefined,
      dropdownClassName: undefined,
      options: ['bold', 'italic', 'underline', 'strikethrough'],
    },
    image: {
      urlEnabled: true,
      urlEnabled: false,
      uploadEnabled: true,
      alignmentEnabled: true,
      uploadCallback: this._uploadImageCallBack,
      previewImage: true,
      inputAccept: 'image/gif,image/jpeg,image/jpg,image/png,image/svg'
    }
  }

  onEditorStateChange = (editorState) => {
    this.setState({editorState});
  };
  
  getImagesForUpload = (editorState) => {
    const entityMap = convertToRaw(this.state.editorState.getCurrentContent()).entityMap; // get images from the editor

    let editorImagesList = this.entityMapToArray(entityMap);
    // console.log('editorImagesList', editorImagesList);

    let filteredImages = [];
    for (let i = 0; i < editorImagesList.length; i++) {
      for (let j = 0; j < this.state.uploadedImages.length; j++) {
        if (editorImagesList[i].data.src === this.state.uploadedImages[j].localSrc) filteredImages.push(this.state.uploadedImages[j])        
      }      
    }

    return filteredImages;
  }

  entityMapToArray = (entityMap) => {
    let arr = [];
    for (const key in entityMap) {
      if (entityMap.hasOwnProperty(key)) {
        arr.push(entityMap[key])
      }
    }
    return arr;
  }

  getUniqUrl = (url) => {
    for (let i = 0; i < this.props.articles.length; i++) {
      if (this.props.articles[i].urlName === url) {
        url += '-' + Date.now();
      }
    }
    return url;
  }

  replaceImagesLinks = (htmlContent, uploadedImages, path) => {
    // console.log('htmlContent', htmlContent)
    let newStr = draftToHtml(convertToRaw(htmlContent))

    for (let i = 0; i < uploadedImages.length; i++) {
      let fileName = uploadedImages[i].file.name;
      let re = new RegExp(uploadedImages[i].localSrc, "g");
      newStr = newStr.replace(re, `${path}/${fileName}`);
    }
    return newStr;
  }

  saveArticle = (e) => {
    e.preventDefault();

    if (!this.state.category) return;
    if (this.state.articleName === '') return this.setState({articleNameError: true});
    if (this.state.articleDescription === '') return this.setState({articleDescriptionError: true})

    let data = new FormData();

    let newUrlArticle = '';
    let uniqArticleUrl = '';
    let categoryUrl = '';
    let userId = '';

    if (this.props.editableArticle) {
      newUrlArticle = this.nameToUrlTranslit(this.state.articleName);
      uniqArticleUrl = this.props.editableArticle.urlName;
      categoryUrl = this.state.category.urlName;
      userId = this.props.editableArticle.userId;
    } else {
      newUrlArticle = this.nameToUrlTranslit(this.state.articleName);
      uniqArticleUrl = this.getUniqUrl(newUrlArticle);
      categoryUrl = this.state.category.urlName;
      userId = getUserIdByUser(this.props.user);
    }

    let path = '/images/articles/' + categoryUrl + '/' + userId + '/' + uniqArticleUrl;
    let contentWithRightLinks = this.replaceImagesLinks(this.state.editorState.getCurrentContent(), this.state.uploadedImages, path);

    contentWithRightLinks = sanitizeHtml(contentWithRightLinks, {
      allowedTags: false,
      allowedAttributes: {
        'img': [ 'src', 'alt', 'style' ],
        'a': ['href']
      }
    })
    

    const content = {
      path: path,
      category: this.state.category,
      articleName: this.state.articleName,
      articleUrl: uniqArticleUrl,
      userId: userId,
      description: this.state.articleDescription,
      html: contentWithRightLinks
    }

    const images = this.getImagesForUpload(this.state.editorState)
    this.setState({uploadedImages: images})
    
    data.append('content', JSON.stringify(content))
    
    for (let i = 0; i < images.length; i++) {
      data.append('image', images[i].file)      
    }

    const { refreshToken } = this.props.user.tokens;

    if (this.props.editableArticle) {
      axios.post('/api/update-article', data, {headers: {'Path-For-Image': path, 'Authorization': `Bearer ${refreshToken}`}})
      .then(response => {
        setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)
        
        this.props.clearEditableArticle();
        this.props.stateInit();
        this.props.history.push('/articles');
      })
      .catch(err => checkAuthorization(err.response.status, this.props.dispatch, userId))
      
    } else {
      axios.post('/api/new-article', data, {headers: {'Path-For-Image': path, 'Authorization': `Bearer ${refreshToken}`}})
      .then(response => {
        setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)

        if (response.data.result.ok === 1) {
          this.props.stateInit();
          this.props.history.push(`/articles/${categoryUrl}`);
        }
      })
      .catch(err => checkAuthorization(err.response.status, this.props.dispatch, userId));
    }
  }

  changeCategory = (e) => {
    const chooseCategory = this.props.articlesCategories.find(item => item._id === e.target.value)
    this.setState({category: chooseCategory})
  }

  renderCategorySelect = () => {
    const options = this.props.articlesCategories.map((category, index) => <option key={index} value={category._id}>{category.name}</option>)

    if (this.state.category) {
      return (
        <select className="select" value={this.state.category._id} onChange={this.changeCategory}>
          {options}
        </select>
      )
    }
    return <div>Loading...</div>    
  }

  changeName = (e) => {
    if (this.state.articleNameError) return this.setState({articleName: e.target.value, articleNameError: false})
    this.setState({articleName: e.target.value});    
  }

  changeDescription = (e) => {
    if (e.target.value.length <= 410) {
      if (this.state.articleDescriptionError) return this.setState({articleDescriptionError: false, articleDescription: e.target.value});

      this.setState({articleDescription: e.target.value});
    }
  }

  nameToUrlTranslit = (name) => {
    let url = name.replace(/[^a-zA-Zа-яА-Я0-9ё\ \-\–\—]/g, '') // только буквы, цифры, тире, пробелы
    return cyrillicToTranslit().transform(url.toLocaleLowerCase(), '-').replace(/'/g, '');
  }

  calcSymbols = (value) => {
    return 410 - value.length;
  }

  render() {
    // console.log('Editor state', this.state)
    const nameError = this.state.articleNameError;
    const descError = this.state.articleDescriptionError;

    return (
      <form action="" encType="multipart/form-data">
        <div className="new-article__select">
          <div className="new-article__select-text">Выберите категорию статьи:</div>       
          {this.renderCategorySelect()}
          <input className="input" value={this.state.articleName} onChange={this.changeName} type="text" placeholder="Название статьи"/>
          {nameError ? <div className="bottom-desc c-danger">Введите название</div> : null}
          <textarea className="input textarea" value={this.state.articleDescription} onChange={this.changeDescription} placeholder="Описание статьи"></textarea>
          <div className="bottom-desc">Доступно символов: {this.calcSymbols(this.state.articleDescription)}</div>
          {descError ? <div className="bottom-desc c-danger">Введите Описание</div> : null}
        </div>
        <Editor editorState={this.state.editorState}
          onEditorStateChange={this.onEditorStateChange}
          toolbar={this.toolbar}/>
        {this.props.editableArticle ? 
          <button className="button btn" onClick={this.saveArticle}>Подтвердить</button>
          :
          <button className="button btn" onClick={this.saveArticle}>Сохранить</button>
        }
        
        {this.state.errorMessage !== '' ? <div>{this.state.errorMessage}</div> : null}
        
      </form>
    )
  }
}

NewArticle.propTypes = {
  articlesCategories: PropTypes.array.isRequired,
  editableArticle: PropTypes.object,
  articles: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired,
  clearEditableArticle: PropTypes.func.isRequired,
  stateInit: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired
}

export default NewArticle;