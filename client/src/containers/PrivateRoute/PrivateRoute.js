import React from 'react';
import { connect } from 'react-redux';
import {Route, Redirect, withRouter} from 'react-router-dom';

import { checkToken } from '../../utils/checkToken';
import Loading from '../Loading/Loading'

class PrivateRoute extends React.Component {
  componentDidMount() {
    this.initToken();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.location.pathname !== nextProps.location.pathname) {
      this.initToken();
    }
  }

  initToken = () => {
    checkToken(this.props.user, this.props.dispatch);
  }
  
  render() {
    const {component: Component, ...rest} = this.props;
    
    if (this.props.user.authOnLoad) {
      return <Loading />
    }
    return (
      <Route {...rest} render={props => {
        if (this.props.user.isAuth) {
          return (
            <Component {...props} rest={rest} />
          )
        } else {
          return (
            <Redirect to={{pathname: '/auth'}}/>
          )
        }
      }}
      />       
    )
  }
}

const mapStateToProps = (state) => {
  return {
    tokens: state.user.tokens,
    user: state.user,
    store: state
  }
}

export default withRouter(connect(mapStateToProps, null)(PrivateRoute));
