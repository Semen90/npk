import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import Loading from '../Loading/Loading';

class Registration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: '',
      confirmPassword: '',
      onLoad: false,
      error: undefined
    }
  }

  handleChange = (e) => {
    const name = e.target.name;
    if (this.state.error) this.setState({error: undefined});

    switch (name) {
      case 'login':
        this.setState({login: e.target.value});
        break;

      case 'password':
        this.setState({password: e.target.value});
        break;

      case 'confirmPassword':
        this.setState({confirmPassword: e.target.value});
        break;

      default:
        break;
    }
  }

  sendForm = (e) => {
    e.preventDefault();    
    const {login, password, confirmPassword} = this.state;

    if (login && password && confirmPassword) {
      if (password !== confirmPassword) return this.setState({error: 'Пароли не совпадают'})

      this.setState({onLoad: true})
      axios.post('/api/registration', {
        login: login,
        password: password
      })
      .then((response) => {
        if (response.data.isLoginAvailable === false) {
          return this.setState({onLoad: false, error: 'Такой логин уже есть'})
        }

        console.log('Пользователь зарегистрирован');
        this.setState({
          login: '',
          password: '',
          confirmPassword: '',
          onLoad: false
        });
        this.props.history.push('/auth');
        return;
      })
      .catch((error) => {
        this.setState({onLoad: false, error: 'Ошибка на сервере. Пользователь не зарегистрирован. Повторите попытку позже'})
        return console.error('Auth error ', error)
      });
    } else {
      return this.setState({error: 'Заполните все поля!'})
    }
  }

  renderForm = () => {
    const {login, password, confirmPassword, onLoad} = this.state;
    if (onLoad) {
      return <Loading />
    }

    return (
      <form className="form" ref={(form) => this.form = form}>
        {this.state.error ? <p className="error text-center">{this.state.error}</p> : null}
        <input 
          className="input-text" 
          placeholder="Логин"
          type="text"
          name="login"
          value={login}
          onChange={this.handleChange} />
        <input
          className="input-text"
          placeholder="Пароль"
          type="password"
          name="password"
          value={password}
          onChange={this.handleChange} />
        <input
          className="input-text"
          placeholder="Повторите пароль"
          type="password"
          name="confirmPassword"
          value={confirmPassword}
          onChange={this.handleChange} />
        <button onClick={this.sendForm} className="form-button button" type="button">Зарегистрироваться</button>
      </form>
    )
  }


  render() {

    return (
      <div className="form-wrap">
        <div className="form-wrap__title">Регистрация</div>
        {this.renderForm()}
      </div>
    );
  }
}

export default withRouter(connect()(Registration));
