import React from 'react';
import PropTypes from 'prop-types'
import axios from 'axios';

import popups from '../../../Popup/popups'
import './fertilizerItem.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import checkAuthorization from '../../../../utils/checkAuthorization'
import { getUserIdByUser } from '../../../../utils/utils';
import { setTokensFromAuthHeader } from '../../../../utils/setTokensFromAuthHeader';

export default class FertilizerItem extends React.Component {
  state = {
    visible: false
  }

  renderComposition = (composition) => {
    return composition.map((ingredient, index) => {
      return (
        <li key={index}>
          <span className="ingredient-list__item-name">{ingredient.name}</span>
          <span className="ingredient-list__item-value">{ingredient.value} %</span> 
        </li>
      )
    })
  }

  editFertilizer = () => {
    
  }

  openPopup = () => {
    this.props.openPopup(popups.EDIT_FERTILIZER, this.props.fertilizer)
  }

  renderCtrls = () => {
    return (
      <div className="fertilizer-item__ctrls">
        <div className="icon" onClick={this.openPopup}>
          <FontAwesomeIcon icon="pen" color="#aaa"/>
        </div>
        <div className="icon" onClick={this.removeFertilizer}>
          <FontAwesomeIcon icon="trash" color="#aaa"/>
        </div>
      </div>
    )
  }

  removeFertilizer = () => {
    this.props.openPopup(popups.REMOVE_FERTILIZER, {}, (result) => {

      if (!result) {
        this.props.closePopup();
        return;
      }

      const { _id } = this.props.fertilizer;
      const { refreshToken } = this.props.user.tokens;
      const userId = getUserIdByUser(this.props.user);


      axios.delete('/api/delete-fertilizer/', {params: {_id: _id}, headers: {'Authorization': `Bearer ${refreshToken}`}})
      .then(response => {
        setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)
        
        if (response.data.ok === 1) {
          this.props.removeFertilizer(_id);
          this.props.closePopup();
        }
      })
      .catch(err => checkAuthorization(err.response.status, this.props, userId))
    }) 
  }

  changeVisibility = (e) => {
    this.setState({visible: !this.state.visible})
  }

  sendFertilizer = (e) => {
    this.props.setFertilizer(this.props.fertilizer);
  }

  render() {
    const item = this.props.fertilizer;
    const { visible } = this.state;
    return (
      <div className={visible ? "fertilizer-item _active" : "fertilizer-item"}>
        <div className="fertilizer-item__top">
          <div className="fertilizer-item__name" onClick={this.changeVisibility}>
            {item.name}
          </div>
          <span className="fertilizer-item__arrow" onClick={this.sendFertilizer}><FontAwesomeIcon icon={['far', 'arrow-alt-circle-right']}/></span>
        </div>
        <div className="fertilizer-item__bottom">
          <ul className={visible ? "ingredient-list" : "ingredient-list"}>
            {this.renderComposition(item.fullComposition)}
          </ul>
          {this.state.visible ? this.renderCtrls() : null}
        </div>
      </div>
    )
  }
}

FertilizerItem.propTypes = {
  openPopup: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
  fertilizer: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  removeFertilizer: PropTypes.func.isRequired,
  setFertilizer: PropTypes.func.isRequired
}