import React from 'react';
import PropTypes from 'prop-types'
import './fertilizer.scss';
import FertilizerItem from './FertilizerItem/FertilizerItem';
import Loading from '../../Loading/Loading'
import popups from '../../Popup/popups';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Fertilizer = (props) => {
  let renderFertilizerList = () => {
    if (props.allFertilizersList.length < 1) {
      return (
        <div className="empty">
         <div className="empty__icon">
            <FontAwesomeIcon icon="flask" />
         </div>
          Добавьте свои удобрения
        </div>
      )
    }

    return props.allFertilizersList.map((item, index) =>
      <FertilizerItem key={item._id}
        fertilizer={item} 
        index={index}
        closePopup={props.closePopup}
        user={props.user}
        dispatch={props.dispatch}
        openPopup={props.openPopup}
        removeFertilizer={props.removeFertilizer} 
        setFertilizer={props.setFertilizer} />
    )
  }

  let openPopup = () => {
    props.openPopup(popups.ADD_FERTILIZER);
  }


  let renderAddButton = () => {
    const { isAuth } = props.user;
    if (isAuth) {
      return (
        <div className="buttons-box">
          <button className="button btn" onClick={openPopup}><i><FontAwesomeIcon icon="plus"/></i>Добавить</button>
        </div>
      )
    }
    return null;
  }

  return (
    <div className="fertilizer">
      <div className="head-text">Удобрения</div>
      <div className="fertilizer-box accordion" id="fertAccordion">
        {props.allFertilizersList ? renderFertilizerList() : <Loading />}
      </div>
      {renderAddButton()}
    </div>
  )
}

Fertilizer.propTypes = {
  allFertilizersList: PropTypes.array,
  user: PropTypes.object.isRequired,
  closePopup: PropTypes.func.isRequired,
  openPopup: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  removeFertilizer: PropTypes.func.isRequired,
  setFertilizer: PropTypes.func.isRequired
}

export default Fertilizer;