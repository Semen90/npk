import React from 'react'
import PropTypes from 'prop-types'
import NumberFormat from 'react-number-format'

import './fertilizersCompositionItem.scss'
import { addDuplicateElements } from '../../../../utils/addDuplicateElements'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class FertilizersCompositionItem extends React.Component {
  state = {
    value: '',
    isNan: false
  }

  componentWillMount = () => {
    this.initValue(this.props.item)
  }
  

  componentWillReceiveProps(nextProps) {
    this.initValue(nextProps.item);
  }

  initValue = (item) => {
    if (item.value) {
      this.setState({value: item.value});
      return;
    }

    this.setState({value: ''})
  }

  formatCalculatedValue = (elementsArray) => {
    return {
      _id: this.props.item._id,
      elements: elementsArray,
      name: this.props.item.name
    }
  }

  increase = () => {
    this.setValue(parseFloat(Number(Number(this.state.value) + 0.01).toFixed(5)));
    
  }
  
  decrease = () => {
    let result = parseFloat(Number((Number(this.state.value) - 0.01).toFixed(5)));
    result = result < 0 ? 0 : result
    this.setValue(result);   
  }

  onChange = (e) => {
    this.setValue(e.target.value)
  }

  setValue = (value) => {
    this.setState({isNan: false});

    if (isNaN(value)) {
      this.setState({isNan: true});
    } else {
      this.setState({value: value}, () => {
        this.props.setFertilizerValue(this.props.item, this.state.value); // fertilizer, value
      })
    }
  }

  mouseWheel = (e) => {
    e.preventDefault();
    
    if (e.deltaY > 0) {
      this.decrease();
      return;
    }
    this.increase();
  }

  calculateComplexElement = (itemValue, elementValue) => {
    const valueFertilizer = this.state.value * 1000;
    return (((valueFertilizer.toFixed(4) * itemValue).toFixed(4) / 100) * elementValue.toFixed(4)) / 100;
  }

  calculateElement = (itemValue) => {
    const valueFertilizer = this.state.value * 1000;
    return (valueFertilizer * itemValue).toFixed(4) / 100;
  }

  calculate = () => {
    const { item } = this.props;
    const { fullComposition } = item;
    let simpleElements = [];
    
    fullComposition.forEach((item, index) => {
      if (item.composition !== undefined) {
        item.composition.forEach((element) => {
          let elementValue = this.calculateComplexElement(item.value, element.value);
          simpleElements.push({
            _id: element._id,
            name: element.name,
            value: elementValue.toFixed(3)
          })
        })
      } else {
        let elementValue = this.calculateElement(item.value);
        simpleElements.push({
          _id: item.id,
          name: item.name,
          value: elementValue.toFixed(3)
        })
      }
    });

    return addDuplicateElements(simpleElements)
  }

  removeElementsComposition = () => {
    this.props.item ? this.props.removeElementsComposition(this.props.item._id) : null;
  }

  render() {
    const { item } = this.props;
    const { value } = this.state;

    return (
      <div className="composition">
        <div className="composition__remove" onClick={this.removeElementsComposition}><FontAwesomeIcon icon="times"/></div>
        <div className="composition__main">
          <div className="composition__name">
            <div>{item.name}</div>
            {this.state.isNan ? <div className="error">Введите число с точкой</div> : null}
          </div>
          <div className="composition-panel">
            <div className="composition-panel__top">
              <div>г/л</div>
              {/* onKeyPress={this.onChange} */}
              <NumberFormat isNumericString={true}
                className="composition__value"
                placeholder="0"
                type="text"
                value={value}
                onWheel={this.mouseWheel}
                onChange={this.onChange} />
            </div>
            <div className="composition-buttons">
              <div className="composition__button _up" onClick={this.increase}><FontAwesomeIcon icon="plus-square" color="#00CC66"/></div>
              <div className="composition__button _down" onClick={this.decrease}><FontAwesomeIcon icon="minus-square" color="#aaa"/></div>
            </div>
            <div></div>
          </div>
        </div>                  
      </div>
    )
  }
}

FertilizersCompositionItem.propTypes = {
  item: PropTypes.object.isRequired,
  setFertilizerValue: PropTypes.func.isRequired,
  removeElementsComposition: PropTypes.func.isRequired
}