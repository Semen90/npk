import React from 'react'
import PropTypes from 'prop-types'

import './fertilizersComposition.scss';
import FertilizersCompositionItem from './FertilizersCompositionItem/FertilizersCompositionItem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class FertilizersComposition extends React.Component {
  state = {
    name: ''
  }

  componentWillReceiveProps(nextProps) {
    
    if (nextProps.mix) {
      this.setState({name: nextProps.mix.name})
      return;
    }

    if (!nextProps.mix && this.state.name !== '') {
      this.setState({name: this.state.name})
    }
  }
  
  setValue = (e) => {

  }

  isDisable = () => {
    return this.state.name.length < 1; 
  }

  renderFertilizers = () => {
    if (this.props.fertilizers.length < 1) {
      return (
        <div className="empty">
         <div className="empty__icon">
            <FontAwesomeIcon icon="vial" />
         </div>
          Добавьте удобрения из списка
        </div>
      )
    }

    return this.props.fertilizers.map((item, index) => 
      <FertilizersCompositionItem key={index} item={item}
        setFertilizerValue={this.props.setFertilizerValue}
        removeElementsComposition={this.props.removeElementsComposition} />
    )
  }

  saveMix = () => {
    this.props.saveMix(this.state.name);
    this.setState({name: ''});
  }
  
  updateMix = () => {
    this.props.updateMix(this.state.name, this.props.mix._id);
    this.setState({name: ''});
  }

  renderCtrls = () => {
    return (
      <form className="">
        <input className="input-text" placeholder="Название раствора"
          value={this.state.name}
          onChange={e => this.setState({name: e.target.value})}/>
        {this.renderButtons()}
      </form>
    )      
  }
  
  renderButtons = () => {
    if (this.props.mix) {
      return (
        <div>
          <button className="button" type="button" onClick={this.updateMix} disabled={this.isDisable()}>
            <i>
              <FontAwesomeIcon icon="sync"/>
            </i>
            Сохранить
          </button>
          <button className="button" type="button" onClick={this.saveMix} disabled={this.isDisable()}>
            <i>
              <FontAwesomeIcon icon="file"/>
            </i>
            Сохранить как новый
          </button>
        </div>
      )
    }

    return (
      <button className="button" type="button" onClick={this.saveMix} disabled={this.isDisable()}>
        <i>
        <FontAwesomeIcon icon="file"/>
        </i>
        Сохранить
      </button>
    )
  }

  render() {
    return(
      <div className="fertilizers-composition">
        <div className="head-text">Состав раствора</div>
        <div className="composition-box">
          {this.renderFertilizers()}
        </div>
        {this.props.fertilizers.length > 0 ? this.renderCtrls() : null}        
      </div>
    )
  }
}

FertilizersComposition.propTypes = {
  fertilizers: PropTypes.array,
  setFertilizerValue: PropTypes.func.isRequired,
  removeElementsComposition: PropTypes.func.isRequired,
  saveMix: PropTypes.func.isRequired,
  updateMix: PropTypes.func.isRequired,
  mix: PropTypes.object
}