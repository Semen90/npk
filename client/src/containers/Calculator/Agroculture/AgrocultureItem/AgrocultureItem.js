import React, { Component } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import popups from '../../../Popup/popups'
import checkAuthorization from '../../../../utils/checkAuthorization'
import { getUserIdByUser } from '../../../../utils/utils'
import { setTokensFromAuthHeader } from '../../../../utils/setTokensFromAuthHeader';


export default class AgrocultureItem extends Component {
  state = {
    isHover: false
  }

  onChoose = () => {
    this.props.setActiveElement(this.props.index);
    this.props.setTableCulture(this.props.item);
  }

  openPopup = () => {
    this.props.openPopup(popups.EDIT_AGROCULTURE, this.props.item)
  }

  removeAgroculture = () => {
    this.props.openPopup(popups.REMOVE_AGROCULTURE, {}, (result) => {
      if (!result) {
        this.props.closePopup();
        return;
      }
      
      const { _id } = this.props.item;
      const { refreshToken } = this.props.user.tokens;
      const userId = getUserIdByUser(this.props.user);

      axios.delete('/api/delete-agroculture/', {params: {_id: _id}, headers: {'Authorization': `Bearer ${refreshToken}`}})
      .then(response => {
        setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)
        
        if (response.data.ok === 1) {
          this.props.removeAgroculture(_id);
          this.props.closePopup();
        }
      })
      .catch(err => checkAuthorization(err.response.status, this.props.dispatch, userId))
    })
  } 

  renderCtrls = () => {
    if (this.state.isHover && this.props.item.userId) {
      return (
        <div className="agroculture-ctrl">
          <div className="icon" onClick={this.openPopup}>
            <FontAwesomeIcon icon="pen" color="#aaa"/>
          </div>
          <div className="icon" onClick={this.removeAgroculture}>
            <FontAwesomeIcon icon="trash" color="#aaa"/>
          </div>
        </div>
      )
    }
  }

  render() {
    const { item } = this.props;
    const { isActive } = this.props;
    return (
      <div className={isActive ? "table__row _active" : "table__row"}
        onMouseEnter={() => this.setState({isHover: true})}
        onMouseLeave={() => this.setState({isHover: false})}
        onClick={this.onChoose}>
        <div className="table__cell _left">{item.name}</div>
        {this.renderCtrls()}
      </div>
    )
  }
}

AgrocultureItem.propTypes = {
  index: PropTypes.number.isRequired,
  item: PropTypes.object.isRequired,
  setActiveElement: PropTypes.func.isRequired,
  setTableCulture: PropTypes.func.isRequired,
  openPopup: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  removeAgroculture: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired
}