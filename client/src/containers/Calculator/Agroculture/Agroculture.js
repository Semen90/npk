import React, { Component } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { CSSTransition } from 'react-transition-group'

import './agroculture.scss';
import AgrocultureItem from "./AgrocultureItem/AgrocultureItem";
import Loading from '../../Loading/Loading'
import Popup from '../../Popup/Popup'
import PopupAddAgroculture from '../../Popup/PopupAddAgroculture/PopupAddAgroculture'
import popups from '../../Popup/popups'
import { getUserIdByUser } from '../../../utils/utils';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class Agroculture extends Component {
  state = {
    agrocultures: undefined,
    activeElement: null
  }

  removeAgroculture = (_id) => {
    let newList = this.state.agrocultures.filter(item => item._id !== _id)
    this.setState({agrocultures: newList})
  }

  setActiveElement = (index) => {
    this.setState({activeElement: index});
  }
  
  componentDidMount() {
    this.setData();
  }
  
  componentWillReceiveProps(nextProps) {
    this.setData();
  }

  setData = () => {
    axios.get('/api/agroculture', { params: {userId: getUserIdByUser(this.props.user)} })
    .then(response => {
      this.setState({agrocultures: response.data})
    })
    .catch(err => {
      console.error('Agroculture', err);
    })
  }

  getEditableAgroculture = (id) => {
    return this.state.agrocultures.filter(item => item._id === id)[0];
  }

  renderVariant = (variant) => {
    return variant.map((item, index) => {
      return <div key={index}>{item.elementName}, {item.value}</div>
    })
  }

  renderAgrocultures = () => {
    return this.state.agrocultures.map((item, index) => {
      let isActive = index == this.state.activeElement ? true : false;

      return <AgrocultureItem 
        index={index} key={index} item={item}
        removeAgroculture={this.removeAgroculture}
        closePopup={this.props.closePopup}
        openPopup={this.props.openPopup}
        user={this.props.user}
        dispatch={this.props.dispatch}
        setTableCulture={this.props.setTableCulture}
        setActiveElement={this.setActiveElement}
        isActive={isActive} />  
    })
  }

  openPopup = () => {
    this.props.openPopup(popups.ADD_AGROCULTURE)
  }

  renderBtn = () => {
    return this.props.user.isAuth ? (
      <div>
        <button className="button btn" onClick={this.openPopup}><i><FontAwesomeIcon icon="plus"/></i>Добавить</button>
        </div>
    )
    : null;
  }

  renderPopup = (props) => {
  }
  
  render() {
    const { isAuth } = this.props.user;
    return (
      <div className="agroculture-block">
        <div className="head-text">Агрокультуры</div>
        <div className="table table-agroculture">
          <div className="table__row _head">
            <div className="table__cell _left">Название культуры</div>
          </div>
          {this.state.agrocultures ? this.renderAgrocultures() : <Loading />}
        </div>
        {this.renderBtn()}
      </div>
    )
  }
}

Agroculture.propTypes = {
  user: PropTypes.object.isRequired,
  closePopup: PropTypes.func.isRequired,
  openPopup: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  setTableCulture: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
}