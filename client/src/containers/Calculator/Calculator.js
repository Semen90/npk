import React from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom';
import { Link } from "react-router-dom";
import axios from 'axios'

import Fertilizer from './Fertilizer/Fertilizer';
import FertilizersComposition from './FertilizersComposition/FertilizersComposition';
import FertilizerTable from './FertilizerTable/FertilizerTable';
import Agroculture from './Agroculture/Agroculture';
import Mixes from './Mixes/Mixes';
import Popup from '../Popup/Popup';
import popups, { popupsType } from '../Popup/popups';
import PopupAddFertilizer from '../Popup/PopupAddFertilizer/PopupAddFertilizer'
import PopupAddAgroculture from '../Popup/PopupAddAgroculture/PopupAddAgroculture'
import PopupRemove from '../Popup/PopupRemove/PopupRemove'
import './calculator.scss';

import { formatFertilizersData } from '../../utils/formatFertilizersData';
import { addDuplicateElements } from '../../utils/addDuplicateElements'
import { checkToken } from '../../utils/checkToken';
import { getUserIdByToken, getUserIdByUser } from '../../utils/utils';
import checkAuthorization from '../../utils/checkAuthorization'
import { setTokensFromAuthHeader } from '../../utils/setTokensFromAuthHeader';

class Calculator extends React.Component {
  state = {
    userId: this.props.user.tokens ? getUserIdByToken(this.props.user.tokens.refreshToken) : null,
    fertilizers: [],
    editableFertilizer: undefined,
    allFertilizersList: undefined,
    mixes: undefined,
    mix: undefined,
    elementsComposition: [],
    allElements: [],
    agroculture: {},
    editableAgroculture: undefined,
    popups: {
      activePopup: undefined,
      list: []
    },
    // removableItem: undefined,
    removableCallback: undefined
  }

  closePopup = () => {
    this.setState({activePopup: undefined, removableCallback: undefined});
  }

  openPopup = (popupName, item, cb) => {
    switch(popupName) {
      case popups.ADD_FERTILIZER:
        this.setState({activePopup: popupName})
        break;
        
      case popups.EDIT_FERTILIZER:
        this.setState({activePopup: popupName, editableFertilizer: item})
        break;
        
      case popups.ADD_AGROCULTURE:
        this.setState({activePopup: popupName})
        break;
        
      case popups.EDIT_AGROCULTURE:
        this.setState({activePopup: popupName, editableAgroculture: item})
        break;

      case popups.REMOVE_FERTILIZER:
        this.setState({activePopup: popupName, removableCallback: cb});
        break;

      case popups.REMOVE_AGROCULTURE:
        this.setState({activePopup: popupName, removableCallback: cb});
        break;

      case popups.REMOVE_MIX:
        this.setState({activePopup: popupName, removableCallback: cb});
        break;

      default: break;
    }
  }

  componentWillMount() {
    this.calcCheckToken();
    this.initMixes();
    this.initAllFertilizers(this.state.userId);
  }

  componentWillReceiveProps(nextProps) {
    this.initMixes();
    this.initAllFertilizers(this.state.userId);
    // this.calcCheckToken();
    // this.setUser();
  }
  
  calcCheckToken = () => {
    checkToken(this.props.user, this.props.dispatch);
  }

  // clearRemovable = () => {
  //   this.setState({removableCallback: undefined})
  // }

  removeMix = (mix) => {
    const { _id } = mix;
    const { refreshToken } = this.props.user.tokens;
    const userId = getUserIdByUser(this.props.user);

    axios.delete('/api/delete-mix', {params: {_id: _id}, headers: {'Authorization': `Bearer ${refreshToken}`}})
    .then(response => {
      setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)
      
      if (response.data.ok === 1) {
        this.removeMixFromState(_id);
        this.closePopup();
      }
    })
    .catch(err => checkAuthorization(err.response.status, this.props.dispatch, userId))
  }

  removeMixFromState = (_id) => {
    const newMixesList = this.state.mixes.filter(item => {
      return item._id !== _id;
    })

    this.setState({mixes: newMixesList});
  }

  initAllFertilizers = (userId) => {
    axios.get('/api/fertilizers', { params: {userId: userId} })
    .then(response => {
      this.setState({allFertilizersList: formatFertilizersData(response.data)});
    })
    .catch(err => {
      console.error('Error /api/fertilizers', err);
    })    
  }

  refreshFertilizerList = () => {
    this.initAllFertilizers(this.state.userId);
    this.setState({activePopup: undefined})
  }

  refreshAgrocultures = () => {
    this.setState({activePopup: undefined, editableAgroculture: undefined});
  }

  initMixes = () => {
    axios.get('/api/mixes', { params: {userId: getUserIdByUser(this.props.user)} })
    .then(response => {
      this.setState({mixes: response.data})
    })
    .catch(err => console.error('Error get mixes', err))
  }

  removeFertilizer = (_id) => {
    let newList = this.state.allFertilizersList.filter(item => {
      return item._id !== _id;
    });
    this.setState({allFertilizersList: newList});
  }

  saveMix = (mixName) => {
    let fertilizers = JSON.parse(JSON.stringify(this.state.fertilizers));
    this.sendMix(this.formatFertilizersForMix(mixName, fertilizers));
  }

  updateMix = (mixName, mixId) => {
    let fertilizers = JSON.parse(JSON.stringify(this.state.fertilizers));
    this.sendMixForUpdate(this.formatFertilizersForMix(mixName, fertilizers), mixId)
  }

  sendMix = (mix) => {
    const userId = getUserIdByUser(this.props.user)
    mix.userId = userId;
    const { refreshToken } = this.props.user.tokens;

    axios.post('/api/add-mix/', mix, {headers: {'Authorization': `Bearer ${refreshToken}`}})
    .then(response => {
      setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)

      if (response.data.result.ok === 1) {
        this.initMixes();
        this.clearFertilizerComposition();
      } else {
        console.log('Не удалось сохранить раствор', mix)
      }
    })
    .catch(err => checkAuthorization(err.response.status, this.props.dispatch, userId))
  }
  
  sendMixForUpdate = (mix, mixId) => {
    let mixData = {
      name: mix.name,
      mixId: mixId,
      userId: getUserIdByUser(this.props.user),
      fertilizers: mix.fertilizers,
    }

    const userId = getUserIdByUser(this.props.user)
    const { refreshToken } = this.props.user.tokens;
    
    axios.post('/api/update-mix/', mixData, {headers: {'Authorization': `Bearer ${refreshToken}`}})
    .then(response => {
      setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)
      
      if (response.data.ok === 1) {
        this.initMixes();
        this.clearFertilizerComposition();
      } else {
        console.log('Не удалось сохранить раствор', mixData);
      }
    })
    .catch(err => checkAuthorization(err.response.status, this.props.dispatch, userId))
  }

  clearFertilizerComposition = () => {
    this.setState({
      mix: undefined,
      fertilizers: [],
      elementsComposition: []
    })
  }

  formatFertilizersForMix = (mixName, fertilizers) => {
    let mix = { name: mixName };
    let newFertilizers = JSON.parse(JSON.stringify(fertilizers));

    mix.fertilizers = newFertilizers.map(item => {
      item.fertilizerId = item._id;

      if (!item.value) {
        item.value = 0;
      }
      delete item.name;
      delete item.fullComposition;
      delete item._id;
      return item;
    })
    return mix;
  }

  editMix = (mix) => {
    let formatted = this.formatEditableMixFertilizers(mix);

    this.setState({mix: formatted, fertilizers: formatted.fertilizers, elementsComposition: []}, () => {
      for (let i = 0; i < this.state.fertilizers.length; i++) {
        this.setFertilizerValue(this.state.fertilizers[i], this.state.fertilizers[i].value)
      }
    });
  }

  formatEditableMixFertilizers = (mix) => {
    let newMix = JSON.parse(JSON.stringify(mix));
    let formattedFertilizers = newMix.fertilizers;

    formattedFertilizers = formattedFertilizers.map(item => {
      item._id = item.fertilizerId;
      item.name = item.fertilizerName;
      delete item.fertilizerId;
      delete item.fertilizerName;

      return item;
    })

    formattedFertilizers.map(fertilizer => {
      this.state.allFertilizersList.forEach(fertilizerFromList => {
        if (fertilizer._id === fertilizerFromList._id) {
          fertilizer.name = fertilizerFromList.name,
          fertilizer.userId = fertilizerFromList.userId,
          fertilizer.fullComposition = fertilizerFromList.fullComposition
        }
      })  
    });

    newMix.fertilizers = formattedFertilizers;
    return newMix;
  }

  setFertilizerValue = (fertilizer, value) => {
    let fertilizers = JSON.parse(JSON.stringify(this.state.fertilizers));
    let newFertilizers = fertilizers.map(item => {
      if (item._id === fertilizer._id) {
        item.value = value;
        return item;
      }
      return item
    })

    this.setState({fertilizers: newFertilizers}, () => {
      let newFertilizer = Object.assign({}, fertilizer);
      newFertilizer.value = value;

      const calculated = this.calculate(newFertilizer);
      const formatted = this.formatCalculatedValue(newFertilizer, calculated);
      this.setDataTable(formatted);
    })
  }

  setFertilizer = (fertilizer) => {
    let doubleElement = [];

    if (this.state.fertilizers.length !== 0) { // поиск дублей удобрений в смешивании
        doubleElement = this.state.fertilizers.filter((item) => {
        return item._id === fertilizer._id
      })
    }   

    if (doubleElement.length <= 0) { // проверка на наличие дублей удобрений в смешивании
      let newCollection = this.state.fertilizers;
      newCollection.push(fertilizer);
      this.setState({fertilizers: newCollection})
    }    
  }

  calculate = (fertilizer, fertilizerValue) => {
    const { fullComposition } = fertilizer;
    let simpleElements = [];
    
    fullComposition.forEach((item, index) => {
      if (item.composition !== undefined) {
        item.composition.forEach((element) => {
          let elementValue = this.calculateComplexElement(item.value, element.value, fertilizer.value);
          simpleElements.push({
            _id: element._id,
            name: element.name,
            value: Number(elementValue.toFixed(3))
          })
        })
      } else {
        let elementValue = this.calculateElement(item.value, fertilizer.value);
        simpleElements.push({
          _id: item.id,
          name: item.name,
          value: Number(elementValue.toFixed(3))
        })
      }
    });

    return addDuplicateElements(simpleElements)
  }

  formatCalculatedValue = (fertilizer, elementsArray) => {
    return {
      _id: fertilizer._id,
      elements: elementsArray,
      name: fertilizer.name
    }
  }

  calculateComplexElement = (itemValue, elementValue, fertilizerValue) => {
    const valueFertilizer = fertilizerValue * 1000;
    return (((valueFertilizer.toFixed(4) * itemValue).toFixed(4) / 100) * elementValue.toFixed(4)) / 100;
  }

  calculateElement = (itemValue, fertilizerValue) => {
    const valueFertilizer = fertilizerValue * 1000;
    return (valueFertilizer * itemValue).toFixed(4) / 100;
  }

  setAllElements = (elements) => {
    this.setState({allElements: elements});
  }

  setTableCulture = (agroculture) => {
    this.setState({agroculture: agroculture})
  }

  setDataTable = (elementsComposition) => {
    let elements = this.state.elementsComposition;
    const filtered = elements.filter(item => elementsComposition._id == item._id);

    if (filtered.length > 0) {
      for (let i = 0; i < elements.length; i++) {
        if (elements[i]._id === filtered[0]._id) elements[i].elements = elementsComposition.elements;
      }

      this.setState({elementsComposition: elements});
      return;
    }

    elements.push(elementsComposition);
    this.setState({elementsComposition: elements});
  }

  removeElementsComposition = (id) => {
    const withoutRemoved = this.state.fertilizers.filter(item => item._id !== id);
    const withoutRemovedComposition = this.state.elementsComposition.filter(item => item._id !== id);

    this.setState({
      fertilizers: withoutRemoved,
      elementsComposition: withoutRemovedComposition
    });

    if (withoutRemoved.length < 1) {
      this.setState({
        mix: undefined
      })
    }
  }  

  render() {
    const { isAuth } = this.props.user;
    // console.log('STATE calc', this.state);
    // console.log(this.props)

    return (
      <div className="calculator">
        {!this.props.user.isAuth ? <div className="main-headertext"><Link to="/registration">Зарегистрируйтесь</Link>, чтобы получить весь функционал калькулятора.</div> : null}
        <div className="calculator-top">
          <Fertilizer
            {...this.props}
            calcCheckToken={this.calcCheckToken}
            closePopup={this.closePopup}
            openPopup={this.openPopup}
            user={this.props.user}
            dispatch={this.props.dispatch}
            setFertilizer={this.setFertilizer}
            removeFertilizer={this.removeFertilizer}
            allFertilizersList={this.state.allFertilizersList} />
          <FertilizersComposition
            saveMix={this.saveMix}
            updateMix={this.updateMix}
            setFertilizerValue={this.setFertilizerValue}
            mix={this.state.mix}
            fertilizers={this.state.fertilizers}
            removeElementsComposition={this.removeElementsComposition} />
          <FertilizerTable
            setAllElements={this.setAllElements}
            composition={this.state.elementsComposition}
            agrocultureTableData={this.state.agroculture} />
          <Agroculture elements={this.state.allElements}
            closePopup={this.closePopup}
            openPopup={this.openPopup}
            setTableCulture={this.setTableCulture}
            calcCheckToken={this.calcCheckToken}
            user={this.props.user}
            dispatch={this.props.dispatch} />
        </div>
        <div className="calculator-bottom">
          <Mixes
            removeMix={this.removeMix}
            closePopup={this.closePopup}
            openPopup={this.openPopup}
            mixes={this.state.mixes}
            editMix={this.editMix}
            user={this.props.user}/>
        </div>
        {/* ============================ popups */}
        {isAuth && this.state.activePopup === popups.ADD_FERTILIZER ?
            <Popup {...this.props} type={popupsType.MEDIUM} closePopup={this.closePopup} title={"Добавить удобрение"}
              popupComponent={<PopupAddFertilizer {...this.props}
                elements={this.state.allElements}
                user={this.state.userId}
                dispatch={this.props.dispatch}
                userTokens={this.props.user.tokens}
                refreshFertilizerList={this.refreshFertilizerList} />}
            />
            : null
          }
          {isAuth && this.state.activePopup === popups.EDIT_FERTILIZER ?
            <Popup {...this.props} type={popupsType.MEDIUM} closePopup={this.closePopup} title={"Редактировать удобрение"}
              popupComponent={
                <PopupAddFertilizer {...this.props}
                  editableFertilizer={this.state.editableFertilizer}
                  user={getUserIdByToken(this.props.user.tokens.refreshToken)}
                  dispatch={this.props.dispatch}
                  userTokens={this.props.user.tokens}
                  elements={this.state.allElements} 
                  refreshFertilizerList={this.refreshFertilizerList}/>
              }
            />
            : null
          }

          {isAuth && this.state.activePopup === popups.ADD_AGROCULTURE ?
            <Popup {...this.props} type={popupsType.MEDIUM} closePopup={this.closePopup} title={"Добавить агрокультуру"}
              popupComponent={<PopupAddAgroculture {...this.props} 
                refreshAgrocultures={this.refreshAgrocultures}
                user={this.state.userId}
                dispatch={this.props.dispatch}
                userTokens={this.props.user.tokens}
                elements={this.state.allElements}/>}
            />
            : null
          }

          {isAuth && this.state.activePopup === popups.EDIT_AGROCULTURE && !this.state.editableAgroculture.readOnly ?
            <Popup {...this.props} type={popupsType.MEDIUM} closePopup={this.closePopup} title={"Редактировать агрокультуру"}
              popupComponent={<PopupAddAgroculture {...this.props}
                refreshAgrocultures={this.refreshAgrocultures}
                setTableCulture={this.setTableCulture}
                user={getUserIdByUser(this.props.user)}
                dispatch={this.props.dispatch}
                userTokens={this.props.user.tokens}
                editableAgroculture={this.state.editableAgroculture}
                elements={this.state.allElements}/>
              }
            />
            : 
            null
          }

          {isAuth && this.state.activePopup === popups.REMOVE_FERTILIZER ?
            <Popup {...this.props} type={popupsType.SMALL}
              closePopup={this.closePopup} title={"Удалить удобрение"}
              popupComponent={<PopupRemove {...this.props}
                removeText={"Вы уверены, что хотите удалить удобрение?"}
                removableCallback={this.state.removableCallback} />}
              />
            :
            null
          }

          {isAuth && this.state.activePopup === popups.REMOVE_AGROCULTURE ?
            <Popup {...this.props} type={popupsType.SMALL}
              closePopup={this.closePopup} title={"Удалить агрокультуру"}
              popupComponent={<PopupRemove {...this.props}
                removeText={"Вы уверены, что хотите удалить агрокультуру?"}
                removableCallback={this.state.removableCallback} />}
              />
            :
            null
          }

          {isAuth && this.state.activePopup === popups.REMOVE_MIX ?
            <Popup {...this.props} type={popupsType.SMALL}
              closePopup={this.closePopup} title={"Удалить раствор"}
              popupComponent={<PopupRemove {...this.props}
                removeText={"Вы уверены, что хотите удалить раствор?"}
                removableCallback={this.state.removableCallback} />}
              />
            :
            null
          }

          {/* ============================ END popups */}
      </div>
    )
  }
}

const mapProps = (state) => {
  return {
    user: state.user
  }
}

Calculator.propTypes = {
  
}

export default connect(mapProps)(Calculator);