import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Loading from '../../Loading/Loading'

import MixesItem from './MixesItem/MixesItem'
import './mixes.scss'

const Mixes = (props) => {
  let renderMixes = () => {
    if (!props.mixes) return <Loading />

    if (props.mixes.length < 1) {
      return (
        <div className="empty">
         <div className="empty__icon">
            <FontAwesomeIcon icon="vials" />
         </div>
          Здесь будут ваши растворы
        </div>
      )
    }

    return props.mixes.map((item, index) => {
      return (
        <MixesItem key={index} mix={item}
          closePopup={props.closePopup}
          openPopup={props.openPopup}
          editMix={props.editMix}
          removeMix={props.removeMix}/>        
      )
    })
  }

  return (
    <div className="mixes">
      <div className="head-text">Готовые растворы</div>
      <div className="mixes-box">
        {props.user.isAuth ? renderMixes() : null}
      </div>
    </div>
  )
}

Mixes.propTypes = {
  mixes: PropTypes.array,
  closePopup: PropTypes.func.isRequired,
  openPopup: PropTypes.func.isRequired,
  editMix: PropTypes.func.isRequired,
  removeMix: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
}

export default Mixes;