import React, { Component } from 'react'
import PropTypes from 'prop-types'
import NumberFormat from 'react-number-format'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import popups from '../../../Popup/popups'

export default class MixesItem extends Component {

  state = {
    isHide: true,
    isHover: false,
    value: '',
    percent: 100
  }

  renderFertilizers = () => {
    return this.props.mix.fertilizers.map((fertilizer, index) => {
      return (
        <div className="mix-fertilizer__item" key={index}>
          <span className="mix-fertilizer__title">{fertilizer.fertilizerName}</span>
          <span className="mix-fertilizer__value">{fertilizer.value} г/л</span>
          <span className="mix-fertilizer__calc _adaptive">{this.calculateFertilizer(fertilizer.value, this.state.value)} г</span>
          {this.state.percent === 100 ?
            null
            :
            <span className="mix-fertilizer__calc _percent _adaptive">{this.calculatePercents(fertilizer.value, this.state.value, this.state.percent)} г ({this.state.percent}%)</span>
          }
        </div>
      )
    })
  }

  calculateFertilizer = (fertilizerValue, value) => {
    let result = fertilizerValue * value;
    if (isNaN(result)) {
      return 'Введите число'
    }
    return Number(Number(result).toFixed(4));
  }

  calculatePercents = (fertilizerValue, value, percent) => {
    let percentDecimal = percent / 100;

    return Number(Number(fertilizerValue * value * percentDecimal).toFixed(4))
  }

  renderCalculated = () => {
    return this.props.mix.fertilizers.map((fertilizer, index) => {
      return (
        <div className="mix-fertilizer__item" key={index}>
          <span className="mix-fertilizer__value">{this.calculateFertilizer(fertilizer.value, this.state.value)} г</span>
          {this.state.percent === 100 ?
            null
            :
            <span className="mix-fertilizer__value _percent">{this.calculatePercents(fertilizer.value, this.state.value, this.state.percent)} г ({this.state.percent}%)</span>
          }
        </div>
      )
    })
  }

  renderPercents = () => {
    return this.props.mix.fertilizers.map((fertilizer, index) => {
      return (
        <div className="mix-fertilizer__item" key={index}>
          <span className="mix-fertilizer__value">{this.calculatePercents(fertilizer.value, this.state.value, this.state.percent)} г</span>
        </div>
      )
    })
  }

  renderInnerPart = () => {
    return (
      <div className="mixes__inner">
        <div className="mix-fertilizer">
          {this.renderFertilizers()}
        </div>
        <div className="mix-calculated">
          {this.renderCalculated()}
        </div>
        {/* {this.state.percent === 100 ?
          null 
          :
          <div className="mix-calculated _percent">
            {this.renderPercents()}
          </div> 
        } */}
        <div className="mix-inputs">
          <div>
            на
            <NumberFormat isNumericString={true}
              placeholder="0"
              className="multiplier-input"
              value={this.state.value}
              onChange={e => this.setState({value: e.target.value})} />
            литров
          </div>
          <div>
            на
            <NumberFormat isNumericString={true}
              placeholder="100"
              className="multiplier-input _percent"
              value={this.state.percent}
              onChange={e => this.setState({percent: e.target.value})} />
            %-ый раствор
          </div>
        </div>
      </div>
    )
  }

  showInner = () => {
    this.setState({isHide: !this.state.isHide})
  }

  editMix = () => {
    this.props.editMix(this.props.mix);
  }

  removeMix = () => {
    this.props.openPopup(popups.REMOVE_MIX, {}, (result) => {
      if (!result) {
        this.props.closePopup();
        return;
      }      
      this.props.removeMix(this.props.mix)

    })
  }

  renderCtrls = () => {
    return (
      <div className="mixes-box__ctrls">
        <div className="icon" onClick={this.editMix}><FontAwesomeIcon icon="pen" color="#aaa"/></div>
        <div className="icon" onClick={this.removeMix}><FontAwesomeIcon icon="trash" color="#aaa"/></div>
      </div>
    )
  }

  render() {
    const { mix } = this.props;

    return (
      <div className={this.state.isHide ? "mixes-box__item" : "mixes-box__item _active"}>
        <div className="mixes-box__top"
          onMouseEnter={() => this.setState({isHover: true})}
          onMouseLeave={() => this.setState({isHover: false})}>
          <div className="mixes-box__title" onClick={this.showInner}>{mix.name}</div>
          {this.renderCtrls()}         
        </div>
        {this.state.isHide ? 
          null
          :
          this.renderInnerPart()          
        }        
      </div>
    )
  }
}

MixesItem.propTypes = {
  mix: PropTypes.object.isRequired,
  editMix: PropTypes.func.isRequired,
  removeMix: PropTypes.func.isRequired,
  openPopup: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired
}