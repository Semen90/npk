import React, { Component } from 'react';
import PropTypes from 'prop-types'

import './fertilizerTableItem.scss';

const FertilizerTableItem = (props) => {
  let element = props.element.length > 0 ? props.element[0].value : 0;

  return (
    <div className="table__row">
      <span className="table__cell _1">{props.item.name}</span>
      <span className="table__cell _2">{element}</span>
      <span className="table__cell _3">{props.vegetation ? props.vegetation.value : 0}</span>
      <span className="table__cell _4">{props.bloom ? props.bloom.value : 0}</span>
    </div>
  )
}

FertilizerTableItem.propTypes = {
  element: PropTypes.array.isRequired,
  item: PropTypes.object.isRequired
}

export default FertilizerTableItem;