import React, { Component } from 'react';
import PropTypes from 'prop-types'
import axios from 'axios';

import './fertilizerTable.scss';
import FertilizerTableItem from "./FertilizerTableItem/FertilizerTableItem";
import { addDuplicateElements } from '../../../utils/addDuplicateElements'
import { getOnlySingleElements } from '../../../utils/utils'

export default class FertilizerTable extends Component {
  state = {
    elements: []
  }

  componentDidMount() {
    this.initTable();
  }
  
  componentWillReceiveProps(nextProps) {
  }


  initTable = () => {
    axios.get('/api/elements', {})
    .then(response => {
      this.props.setAllElements(response.data);
      this.setState({elements: getOnlySingleElements(response.data)})
    })
    .catch(err => {
      console.error(err)
    })
  }

  combineComposition = (composition) => {
    if (composition.length <= 0) return;
    let elements = []


    composition.forEach(comp => {
      comp.elements.forEach(elem => {
        elements.push(elem);
      })
    })    
    // console.log('elements', elements);
    // console.log('Итого', addDuplicateElements(elements));
    return addDuplicateElements(elements);
  }

  renderElements = (combinedElements, agrocultureTableData) => {
    let filtered = [];
    let filteredVegetation = [];
    let filteredBloom = [];

    return this.state.elements.map((item, index) => {
      if (combinedElements && combinedElements.length > 0) {
        filtered = combinedElements.filter(comb => item._id === comb._id);        
      }

      if (agrocultureTableData.vegetation && agrocultureTableData.vegetation.length > 0) {
        filteredVegetation = agrocultureTableData.vegetation.filter(vega => item._id === vega.elementName);
      }

      if (agrocultureTableData.bloom && agrocultureTableData.bloom.length > 0) {
        filteredBloom = agrocultureTableData.bloom.filter(bloom => item._id === bloom.elementName);
      }

      return <FertilizerTableItem key={index} item={item} element={filtered} vegetation={filteredVegetation[0]} bloom={filteredBloom[0]} />
    }); 
  }  

  renderTable = () => {
    let composition = [];
    let agrocultureTableData = {};

    if (this.props.composition.length > 0) {
      composition = this.props.composition;
      // return this.renderElements(this.combineComposition(composition)) // если есть готовые добавленные удобрения
    }

    if (this.props.agrocultureTableData) {
      agrocultureTableData = this.props.agrocultureTableData;
    }

    // console.log(composition)
    // console.log(agrocultureTableData)
    return this.renderElements(this.combineComposition(composition), agrocultureTableData);
  }

  render() {
    return (
      <div className="table-wrap table-elements _flex">
        <div className="head-text">Таблица смесей</div>
        <div className="table">
          <div className="table__row _head">
            <span className="table__cell _1"><span className="_db">Элемент</span></span>
            <span className="table__cell _2"><span className="_db">Замешано</span><span className="_db">мг/л</span></span>
            <span className="table__cell _3"><span className="_db">Вегетация</span><span className="_db">мг/л</span></span>
            <span className="table__cell _4"><span className="_db">Цветение</span><span className="_db">мг/л</span></span>
          </div>
          {this.renderTable()}
        </div>
      </div>
    )
  }
}

FertilizerTable.propTypes = {
  setAllElements: PropTypes.func.isRequired,
  composition: PropTypes.array.isRequired,
  agrocultureTableData: PropTypes.object.isRequired
}