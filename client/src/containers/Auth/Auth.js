import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import './auth.scss';
import { fetchAuth } from '../../actions/user';
import Loading from '../Loading/Loading'
// import { setStoreToLocalStorage } from '../../utils/utils';

class Auth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: '',
      error: undefined,
    }
  }


  handleChange = (e) => {
    if (this.state.error) this.setState({error: undefined})

    const name = e.target.name;
    switch (name) {
      case 'login':
        this.setState({login: e.target.value});
        break;

      case 'password':
        this.setState({password: e.target.value});
        break;

      default:
        break;
    }
  }

  sendForm = (e) => {
    e.preventDefault();
    const { dispatch } = this.props;
    const { login, password } = this.state;

    if (login === '' || password === '') {
      return this.setState({error: 'Заполните оба поля'})
    }
    
    dispatch(fetchAuth(login, password, (err) => {
      if (err) return this.setState({error: 'Неверный логин или пароль.'})
      this.setState({login: '', password: ''});
      // setStoreToLocalStorage(this.props.store);
      this.props.history.push('/articles');
    }));
  }

  renderForm = () => {
    const {login, password} = this.state;

    if (this.props.authOnLoad) {
      return (
        <Loading />
      )
    }

    return (
      <form className="form">
        {this.state.error ? <div className="error text-center">{this.state.error}</div> : null}
        <input 
          className="input-text" 
          placeholder="Логин"
          type="text"
          name="login"
          value={login}
          onChange={this.handleChange} />
        <input
          className="input-text"
          placeholder="Пароль"
          type="password"
          name="password"
          value={password}
          onChange={this.handleChange} />
        <button onClick={this.sendForm} className="form-button button" type="button">Войти</button>
      </form>
    )
  }

  render() {
    return (
      <div className="form-wrap">
        <div className="form-wrap__title">Введите логин и пароль</div>
        {this.renderForm()}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authOnLoad: state.user.authOnLoad,
    store: state
  }
}

Auth.propTypes = {
  dispatch: PropTypes.func,
  authOnLoad: PropTypes.bool
}

export default withRouter(connect(mapStateToProps, null)(Auth));