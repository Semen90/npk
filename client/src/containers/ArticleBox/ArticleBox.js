import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link, NavLink, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import Parser from 'html-react-parser'
import axios from 'axios'

import './ArticleBox.scss'
import Article from '../Article/Article'
import NewArticle from '../NewArticle/NewArticle'
import Loading from '../Loading/Loading'
import { formatDate } from '../../utils/utils'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class ArticleBox extends Component {
  state = {
    articles: [],
    articlesCategories: [],
    editableArticle: undefined,
    currentArticle: {},
    onLoad: false
  }
  
  componentWillMount = () => {
    this.stateInit()    
  }

  // componentWillReceiveProps(nextProps) {
  //   this.stateInit()
  // }

  stateInit = () => {
    this.setState({onLoad: true})

    axios.get('/api/articles')
    .then((response) => {
      let articles = response.data;
      articles = articles.sort(this.sortArticlesByDate)
      this.setState({articles: response.data})
    })
    .catch((error) => {
      console.error(error);
    });

    axios.get('/api/articles-categories')
    .then(response => {
      this.setState({articlesCategories: response.data, onLoad: false})
    })
    .catch(err => {
      this.setState({onLoad: false})
      console.error('Api articles categories error', err)
    })
  }

  sortArticlesByDate = (a, b) => a.date < b.date;

  setEditableArticle = (editableArticle) => {
    this.setState({editableArticle: editableArticle}, function() {
      this.props.history.push('/articles/new-article')
    })
  }

  clearEditableArticle = () => {
    this.setState({editableArticle: undefined})
  }


  setCurrentArticle = (e) => {
  }

  getCategoryById = (id) => {
    if (this.state.articlesCategories.length > 1) {
      return this.state.articlesCategories.find(category => category._id === id);
    }
  }

  renderPreviews = (categoryUrlName) => {
    const { location } = this.props;
    
    if (this.state.articlesCategories.length >= 1) {
      const filteredCategory = this.state.articlesCategories.find(category => category.urlName === categoryUrlName);

      if (this.state.articles.length >= 1) {
        const filteredArticles = this.state.articles.filter(article => article.categoryId === filteredCategory._id);

        if (filteredArticles.length >= 1) {
          return this.renderPrev(filteredArticles, location);
        }
      }
    }

    return (<div className="text-empty">В этой категории еще нет статей</div>);
  }

  renderWithoutCategory = () => {
    const { location } = this.props;

    if (this.state.articles.length >= 1) {
      let articles = this.state.articles.slice(0, 2);
      return this.renderPrev(articles, location, true);
    }

    return null
  }

  renderPrev = (articles, location, withoutCategory) => {
    
    if (this.state.articlesCategories.length > 1) {
      return articles.map((article) => {
        // console.log('article', article)
        const category = this.getCategoryById(article.categoryId);
        let path = withoutCategory ? 'articles/' + category.urlName : location.pathname;
  
        return(
          <div className="article-box__preview" key={article._id}>
            <div className="article-box__preview__top">
              <Link onClick={this.setCurrentArticle} className="article-box__title" to={path + '/' + article.urlName}>{article.title}</Link>
              <div className="article-box__date">{formatDate(article.date)}</div>          
            </div>
            <div className="article-box__content">
              {article.description + ' '}
              <Link to={path + '/' + article.urlName}>читать далее...</Link>
            </div>          
          </div>
        );
      })
    }

    return null;
  }

  getArticleById = (id) => {
    if (this.state.articles) {
      return this.state.articles.find(article => article._id === id);
    }
  }
  
  getArticleByUrlName = (urlName) => {
    if (this.state.articles) {
      return this.state.articles.find(article => article.urlName === urlName);
    }
  }

  renderCategories = () => {
    if (this.state.articlesCategories.length >= 1) {
      const categories = this.state.articlesCategories.map((item, index) => {
        return (
          <li key={index} className="sidebar-list__item">
            <NavLink activeClassName="_active" to={'/articles/' + item.urlName}>{item.name}</NavLink>
          </li>
        )       
      });
      return categories;
    }
  }

  render() {
    // console.log('BOX editableArticle state', this.state.editableArticle)
    // console.log('BOX this.props', this.props)
    if (!this.state.articles) {
      return <div></div>
    }

    return (
      <div className="articles-page">
        <Route path="/articles">
          <aside className="sidebar">
            <div className="sidebar__title head-text">Категории</div>
            <ul className="sidebar-list">
              {this.state.onLoad ? <Loading /> : null}
              {this.renderCategories()}
            </ul>
            <div>
              <Link to="/articles/new-article" className="button btn _center" type="button"><FontAwesomeIcon icon={['far', 'edit']}/>Написать статью</Link>
            </div>
          </aside>
        </Route>
        <div className="article-box">
          <Switch>
            <Route path="/articles/new-article" render={props =>
              <NewArticle {...props}
                articles={this.state.articles}
                stateInit={this.stateInit}
                articlesCategories={this.state.articlesCategories}
                getCategoryById={this.getCategoryById}
                editableArticle={this.state.editableArticle}
                clearEditableArticle={this.clearEditableArticle}
                dispatch={this.props.dispatch}
                user={this.props.user} />}
            />
            <Route exact path="/articles" render={props => {return this.renderWithoutCategory();}} />
            <Route exact path="/articles/:category" render={(props) => {return this.renderPreviews(props.match.params.category);}} />
            <Route path="/articles/:category/:urlName" render={(props) => <Article {...props} user={this.props.user} 
              article={this.getArticleByUrlName(props.match.params.urlName)}
              stateInit={this.stateInit}
              setEditableArticle={this.setEditableArticle}
              getCategoryById={this.getCategoryById}
              openPopup={this.props.openPopup}
              dispatch={this.props.dispatch}
              closePopup={this.props.closePopup} />} />
          </Switch>
        </div>
      </div>
    )    
  }
}

ArticleBox.propTypes = {
  dispatch: PropTypes.func,
  user: PropTypes.object,
  rest: PropTypes.object
}

const mapState = (state) => {
  return {
    user: state.user
  }
}

export default connect(mapState)(ArticleBox);

