import React, { Component } from 'react';
import PropTypes from 'prop-types'
import axios from 'axios'
import Parser from 'html-react-parser';

import './Article.scss';
import NoMatch from '../NoMatch/NoMatch'
import { formatDate, getUserByToken } from '../../utils/utils'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import popups, { popupsType } from '../Popup/popups';
import checkAuthorization from '../../utils/checkAuthorization'
import { setTokensFromAuthHeader } from '../../utils/setTokensFromAuthHeader';

class Article extends Component {
  state = {
    removingError: false
  }

  removeArticle = () => {
    console.log('on remove')
    this.props.openPopup(popups.REMOVE_ARTICLE, {}, (result) => {
      if (!result) {
        this.props.closePopup();
        return;
      }
      
      // const { _id } = this.props.item;
      const articleRemoved = {
        articleId: this.props.article._id,
        articleName: this.props.article.urlName,
        categoryUrl: this.props.getCategoryById(this.props.article.categoryId).urlName,
        userId: this.props.article.userId
      };
      const { refreshToken } = this.props.user.tokens;

      axios.delete('/api/delete-article/', {params: {articleRemoved: articleRemoved}, headers: {'Authorization': `Bearer ${refreshToken}`}})
      .then(response => {
        setTokensFromAuthHeader(response.headers.authorization, this.props.dispatch)
        
        if (response.data.ok === 1) {
          this.props.stateInit();
          this.props.closePopup();
          this.props.history.push('/articles');
        }
      })
      .catch(err => {
        this.setState({removingError: true})
        this.props.closePopup();
        checkAuthorization(err.response.status, this.props.dispatch, this.props.article.userId)
        console.error('Remove agroculture error', err)
      })
    })
  }

  // editArticle = () => {
  //   // console.log(this.props)
  //   this.props.setEditableArticle(this.props.article)
  // }

  renderCtrls = () => {
    const { article } = this.props;
    if (this.props.user.tokens) {
      const user = getUserByToken(this.props.user.tokens.refreshToken)
    
      if (article.userId === user.userId) {
        return (
          <div className="article-full__ctrls">
            {/* <div className="icon" onClick={this.editArticle}>
              <FontAwesomeIcon icon="pen" color="#aaa"/>
            </div> */}
            <div className="icon" onClick={this.removeArticle}>
              <FontAwesomeIcon icon="trash" color="#aaa"/>
            </div>
          </div>
        )
      }
    }
    

    return null;

  }

  render() {
    const { article } = this.props;
    return(
      article ?
        (
        <div className="article-full">
          {this.state.removingError ? (<div className="c-danger">Не получилось удалить. Попробуйте позже</div>) : null}
          <div className="article-full__top">
            <h1 className="article-full__title">{article.title}</h1>
            {this.renderCtrls()}
          </div>
          <div className="article-full__content">
            {Parser(article.content)}
          </div>
          <div className="article-full__date">{formatDate(article.date)}</div>
          {/* <div className="article-full__date">{formatDate(article.date)}{' '}{user.userId}</div> */}
        </div>  
        )
        :
        <NoMatch />
    );
  }
}

Article.propTypes = {
  openPopup: PropTypes.func,
  closePopup: PropTypes.func,
  getCategoryById: PropTypes.func,
  stateInit: PropTypes.func,
  dispatch: PropTypes.func,
  user: PropTypes.object,
  article: PropTypes.object
}

export default Article;