import React, { Component } from 'react';
import {Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Home from '../containers/Home/Home'
import Header from '../containers/Header/Header';
import Auth from '../containers/Auth/Auth';
import ArticleBox from '../containers/ArticleBox/ArticleBox';
import Registration from '../containers/Registration/Registration';
import NoMatch from '../containers/NoMatch/NoMatch';
import PrivateRoute from '../containers/PrivateRoute/PrivateRoute';
import Calculator from '../containers/Calculator/Calculator';
import Popup from '../containers/Popup/Popup'
import PopupRemove from '../containers/Popup/PopupRemove/PopupRemove'
import popups, { popupsType } from '../containers/Popup/popups';

import '../fonts.scss'
import './App.scss'
import './icons/icons'

class App extends Component {

  state = {
    activePopup: undefined,
    removableCallback: undefined
  }

  openPopup = (popupName, item, cb) => {
    switch(popupName) {
      case popups.REMOVE_ARTICLE:
        this.setState({activePopup: popupName, removableCallback: cb});
        break;

      default: break;
    }
  }

  closePopup = () => {
    this.setState({activePopup: undefined, removableCallback: undefined});
  }

  render() {
    return(
      <div className="wrapper">
        {this.state.activePopup === popups.REMOVE_ARTICLE ?
          <Popup {...this.props} type={popupsType.SMALL}
            closePopup={this.closePopup} title={"Удалить статью"}
            popupComponent={<PopupRemove {...this.props}
              removeText={"Вы уверены, что хотите удалить статью?"}
              removableCallback={this.state.removableCallback} />}
            />
          :
          null
        }
        <Header/>
        <div className="main-content">
          <div className="inner">
            <Switch>
              <Route exact path="/" render={(props) => <Home {...props} greetings="hi!" />} />
              {/* <PrivateRoute path="/articles" closePopup={this.closePopup} openPopup={this.openPopup} component={ArticleBox} /> */}
              <Route path="/articles" render={props => <ArticleBox closePopup={this.closePopup} openPopup={this.openPopup} {...props}/>}/>
              <Route path="/calculator" component={Calculator} />
              <Route path="/auth" component={Auth} />
              {this.props.store.user.isAuth ? null : (<Route path="/registration" component={Registration} />)}              
              <Route component={NoMatch} />
            </Switch>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    store: state
  }
}

export default withRouter(connect(mapStateToProps, null)(App));

// todo: сделать кастомный скролл