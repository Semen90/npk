import { library } from '@fortawesome/fontawesome-svg-core'
import { 
  faStroopwafel,
  faPlus, 
  faPlusCircle, 
  faSearchPlus, 
  faPencilAlt,
  faPen, 
  faTrash,
  faPlusSquare,
  faMinusSquare,
  faVial,
  faVials,
  faSync,
  faFile,
  faFlask,
  faTimesCircle,
  faBars,
  faTimes
 } from '@fortawesome/free-solid-svg-icons';
import { faArrowAltCircleRight, faEdit } from '@fortawesome/free-regular-svg-icons';

export default library.add([
  faStroopwafel,
  faPlus,
  faFile,
  faPlusCircle,
  faSearchPlus,
  faTrash,
  faVial,
  faVials,
  faTimes,
  faFlask,
  faBars,
  faSync,
  faTimesCircle,
  faArrowAltCircleRight,
  faEdit,
  faPlusSquare,
  faMinusSquare,
  faPen,
  faPencilAlt
])