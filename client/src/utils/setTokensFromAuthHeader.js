import { authSuccess } from '../actions/user'

export const setTokensFromAuthHeader = (jsonTokens, dispatch) => {
  let tokens = JSON.parse(jsonTokens)
  dispatch(authSuccess(tokens));
}