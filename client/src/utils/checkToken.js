import jwt from 'jsonwebtoken';
import axios from 'axios';
import { setTokens, tokensStarted, tokensFailed, tokensSuccess, removeTokens } from '../actions/user';



export const checkToken = (user, dispatch, cb) => {

  if (!user.tokens) {
    dispatch(tokensFailed());
    return;
  }
  
  if (isTokenValid(user.tokens.accessToken)) {
    return;
  }

  if (!user.authOnLoad) {
    if (isTokenValid(user.tokens.refreshToken)) {

      dispatch(tokensStarted());
      
      axios.post('/api/auth/refreshToken', {
        refreshToken: user.tokens.refreshToken
      })
      .then(response => {
        dispatch(tokensSuccess(response.data))
      })
      .catch(error => {
        dispatch(tokensFailed());
        console.error('Error - refresh token', error);
      });
    }
    else {
      dispatch(tokensFailed());
    }
  }
}

function isTokenValid(token) {
  try {
    const {exp, iat} = jwt.decode(token);
    const now = new Date / 1000;            
    return now < exp + iat;
  }
  catch(err) {
    console.error(err);
    return false;
  }    
}