export const addDuplicateElements = (elements) => {
  let elementsArr = elements.concat();

  let addedElements = [];
  addedElements.push({_id: elementsArr[0]._id, name: elementsArr[0].name, value: 0});

  for (let i = 0; i < elementsArr.length; i++) {  // поиск используемых элементов
    let isElementHere = false;

    for (let j = 0; j < addedElements.length; j++) {
      if (elementsArr[i]._id === addedElements[j]._id) {
        isElementHere = true;
      }
    }

    if (!isElementHere) {
      addedElements.push({_id: elementsArr[i]._id, name: elementsArr[i].name, value: 0});
    }
  }

  addedElements.forEach((item) => { // сложение используемых элементов
    for (let i = 0; i < elementsArr.length; i++) {
      if (item._id === elementsArr[i]._id) {
        item.value = Number((Number(item.value.toFixed(4)) + Number(elementsArr[i].value.toFixed(4))).toFixed(4)); 
      }       
    }
  })
  
  return addedElements;
}