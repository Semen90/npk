import axios from 'axios'
import jwt from 'jsonwebtoken'

export const setStoreToLocalStorage = function(store) {
  localStorage.setItem('app-store', JSON.stringify(store));
}

export const getLocalStorageValue = function() {
  try {
    const localStorageValue = localStorage.getItem('app-store');
    if (!localStorageValue) return;
    return JSON.parse(localStorageValue);

  } catch(err) {
    console.error('getLocalStorageValue error', err);
    return;
  }
}

export const getUserIdByToken = (token) => token ? jwt.decode(token).userId : null;

export const getUserByToken = (token) => token ? jwt.decode(token) : null;

export const getUserIdByUser = (user) => {
  if (user.tokens) {
    return jwt.decode(user.tokens.refreshToken).userId;
  }
  return null;
}

export const removeStoreFromLocalStorage = function() {
  localStorage.removeItem('app-store');
}

export const removeTokens = function() {
  const tokens = JSON.parse(localStorage.getItem('tokens'));

  if(!!tokens) {
    axios.post('/api/auth/removeToken', {token: tokens.refreshToken})
      .then(response => {
        if(response.status === 200) {
          localStorage.removeItem('tokens');
        }
      })
      .catch(err => {
        console.error(err);
      })
  }  
}

export const getOnlySingleElements = (allElements) => {
  return allElements.filter(item => {
    return item.composition === undefined;
  })
}

export const formatDate = (dateStr) => {
  let date = new Date(dateStr);
  let day = formatForZero(date.getDate());
  let month = formatForZero(date.getMonth() + 1);
  
  return `${day}.${month}.${date.getFullYear()}`;
}

const formatForZero = (number) => {
  return number < 10 ? '0' + number : number;
}

// const storeFromLocalStorage = () => {
//   try {
//     const serialized = localStorage.getItem('app-store');
//     if (!serialized) return;
//     return JSON.parse(serialized);

//   } catch (err) {
//     console.error(err);
//     return;
//   }
// }


// export const setTokens = function(tokens){
//   localStorage.setItem('tokens', JSON.stringify(tokens));
// }