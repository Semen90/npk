export const formatFertilizersData = (fertilizersObj) => {
  const fertilizers = JSON.parse(JSON.stringify(fertilizersObj));

  for (let i = 0; i < fertilizers.length; i++) {
    for (let j = 0; j < fertilizers[i].fullComposition.length; j++) {
      for (let k = 0; k < fertilizers[i].elementOptions.length; k++) {
        if (fertilizers[i].fullComposition[j].id === fertilizers[i].elementOptions[k]._id) {

          fertilizers[i].fullComposition[j].name = fertilizers[i].elementOptions[k].name;
          fertilizers[i].fullComposition[j].composition = fertilizers[i].elementOptions[k].composition;

        }        
      }
    }
    delete fertilizers[i].elementOptions;
  }
  return fertilizers;
}