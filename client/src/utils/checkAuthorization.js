import { removeTokens, fetchRemoveTokens } from '../actions/user'

export default (status, props, userId) => {
  if (status >= 500) return console.log('Ошибка на сервере. Повторите позже');

  if (status >= 400 && status < 500) {
    props.dispatch(fetchRemoveTokens(userId))
    return props.history.push('/auth');
  }
}
