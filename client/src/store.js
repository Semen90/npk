import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'react-router-redux';
import { createStore, applyMiddleware } from 'redux';

import createHistory from "history/createBrowserHistory";

import reducer from './reducers/index';
import { getLocalStorageValue, setStoreToLocalStorage } from './utils/utils'

export const history = createHistory();
const rMiddleware = routerMiddleware(history);


const store = createStore(
  reducer,
  getLocalStorageValue(),
  composeWithDevTools(
    applyMiddleware(thunk, rMiddleware)
  )
);


store.subscribe(() => {
  const currentState = store.getState();
  setStoreToLocalStorage(currentState);
  
  // const localStorageValue = getLocalStorageValue();
  // // console.log('*****************************************')
  // // console.log('STORE.SUBS localStorageValue', localStorageValue)
  // // console.log('STORE.SUBS currentState', currentState)

  // if (!currentState.accessToken || !currentState.refreshToken) return;

  // if (!!localStorageValue) {
  //   const oldTokens = localStorageValue.user.tokens;
  //   const newTokens = currentState.user.tokens;

  //   // console.log('STORE.SUBS oldTokens', oldTokens)
  //   // console.log('STORE.SUBS newTokens', newTokens)
  //   // console.log('*****************************************')
    
  //   if (oldTokens.accessToken !== newTokens.accessToken && oldTokens.refreshToken !== newTokens.refreshToken) {
  //     setStoreToLocalStorage(currentState);
  //   }
  // }
});


export default store;