import axios from 'axios';
import { removeStoreFromLocalStorage } from '../utils/utils'

export const actions = {
  FETCH_AUTH_START: 'FETCH_AUTH_START',
  FETCH_AUTH_FAIL: 'FETCH_AUTH_FAIL',
  FETCH_AUTH_SUCCESS: 'FETCH_AUTH_SUCCESS',

  FETCH_TOKENS_START: 'FETCH_TOKENS_START',
  FETCH_TOKENS_SUCCESS: 'FETCH_TOKENS_SUCCESS',
  FETCH_TOKENS_FAIL: 'FETCH_TOKENS_FAIL',

  SET_TOKENS: 'SET_TOKENS',
  REMOVE_TOKENS: 'REMOVE_TOKENS'
}



export const removeTokens = () => {
  return { type: actions.REMOVE_TOKENS }
}

export const tokensStarted = () => {
  return { type: actions.FETCH_TOKENS_START }
}

export const tokensSuccess = (tokens) => {
  return { type: actions.FETCH_TOKENS_SUCCESS, tokens }
}

export const tokensFailed = () => {
  return { type: actions.FETCH_TOKENS_FAIL }
}

// export const setTokens = (tokens) => {
//   return { type: actions.SET_TOKENS, tokens }
// }

export const fetchRemoveTokens = (userId) => {
  return dispatch => {
    if (!!userId) {
      // dispatch(authStarted());

      axios.post('/api/auth/removeToken', {userId: userId})
      .then(response => {
        if(response.status === 200) {
          dispatch(removeTokens());
          removeStoreFromLocalStorage();
        }
      })
      .catch(err => {
        console.error(err);
      })
    }    
  }
}

export const authStarted = () => {
  return { type: actions.FETCH_AUTH_START }
}

export const authFailed = () => {
  return { type: actions.FETCH_AUTH_FAIL }
}

export const authSuccess = (tokens) => {
  return { type: actions.FETCH_AUTH_SUCCESS, tokens }
}

export const fetchAuth = (login, password, cb = () => {}) => {
  return dispatch => {
    dispatch(authStarted());

    axios.post('/api/auth', {
      login,
      password
    })
    .then((response) => {
      // console.log('Ответ на аутентификацию', response.data);
      dispatch(authSuccess(response.data));
      cb();
    })
    .catch((error) => {
      dispatch(authFailed(error));
      cb(error);
      console.error('Auth error ', error)
    });
  }
}