const initialState = {
  authOnLoad: false,
  isAuth: false,
  tokens: null
};

import { actions } from '../actions/user';

const userReducer = (state = initialState, action) => {
  switch(action.type) {
    case actions.FETCH_AUTH_FAIL: 
      return {...state, isAuth: false, authOnLoad: false, tokens: null}

    case actions.FETCH_AUTH_START: 
      return {...state, authOnLoad: true}

    case actions.FETCH_AUTH_SUCCESS: 
      return {...state, isAuth: true, authOnLoad: false, tokens: action.tokens}

    case actions.FETCH_TOKENS_START: 
      return {...state, authOnLoad: true}

    case actions.FETCH_TOKENS_SUCCESS: 
      return {...state, authOnLoad: false, isAuth: true, tokens: action.tokens}


    /* * одиннаковые действия * */
    
    case actions.FETCH_TOKENS_FAIL: 
    return {...state, authOnLoad: false, isAuth: false, tokens: null}
    
    case actions.REMOVE_TOKENS:
    return {...state, authOnLoad: false, isAuth: false, tokens: null}
    
    /* * одиннаковые действия * */


    // case actions.SET_TOKENS:
    //   return {...state, }

  }
  return state;
}

export default userReducer;