import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import userReducer from './user';

const reducer = combineReducers({
  user: userReducer,
  router: routerReducer
})

export default reducer;