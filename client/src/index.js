import React from 'react';
import { render } from 'react-dom';
// import { BrowserRouter, withRouter } from "react-router-dom";
import { Provider } from 'react-redux';

import { ConnectedRouter, routerMiddleware } from 'react-router-redux';

import App from './App/App';
import './index.scss'
import store, { history } from './store';

class Index extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </Provider>
    )
  }
}

render(<Index />, document.getElementById('root'));